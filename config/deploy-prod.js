var path = require('path');
var WebpackFtpUpload = require('webpack-ftp-upload-plugin');
const Dotenv = require('dotenv-webpack');
var dotenv = require('dotenv').config({path: __dirname + '/.env'});
 
module.exports = {
    plugins: [
        new WebpackFtpUpload({
            host: '',
            port: '',
            username: '',
            password: '',
            local: path.join(__dirname, process.env.DIST),
            path: '',
        })
    ]
}
