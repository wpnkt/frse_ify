const path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = {
 // mode: "production",
  mode: "development",
  entry: "./src/assets/jsBundle/index.js",
  output: {
    path: path.resolve(__dirname, "build/wordpress/wp-content/themes/wordpressify/"),
    filename: "index.js"
  },
  plugins: [
    new Dotenv()
  ],
  watch: true,
  module: {
    rules : [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.png$/,
        use: ['file-loader'],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: 'fonts/'
            }
        }]
      }
    ]
  }
}
