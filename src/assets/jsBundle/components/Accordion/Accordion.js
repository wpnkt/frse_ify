import React from 'react';
import ReactDOM from 'react-dom';

class Accordion extends React.Component{

    state = {
        fold: false
    };

    headerClickHandler = () => {
        this.setState({
            fold: !this.state.fold
        });
    };

    render(){

        let tabs = (<h1>rozwiń...</h1>);
        if( this.props.tabs ){
            tabs = this.props.tabs.map( (tab, key) => {
                return (<div key={key} className="accordion__tab">
                    {tab}
                </div>);
            });
        }

        return (<div className={"accordion " + "is-fold-"+this.state.fold} >
            <div className="accordion__header" onClick={this.headerClickHandler} >
                <div className="accordion__tabs">
                    { tabs }
                </div>
            </div>
            <div className="accordion__primary">
                { this.props.children }
            </div>
        </div>);

    }

}

export default Accordion;
