import React from 'react';
const axios = require('axios');

import Accordion from '../Accordion/Accordion';
import Table from '../Table/Table';

class ShippingList extends React.Component{

    state= {
        shippings: []
    };

    componentDidMount(){
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','shippings');
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                let data = response.data;
                console.log( data );
            })
            .catch( (error) => {
                console.log(error);
            })
    }

    getReceipt(){
        return {
            id: this.rand(1,100),
            name: "Przsykla 0912873",
            count: 10,
            price: 100,
            status: "Oczekuje",
            link: 'www.test.pl'
        };
    }

    rand( min, max ){
        min = parseInt( min, 10 );
        max = parseInt( max, 10 );
        if ( min > max ){
            var tmp = min;
            min = max;
            max = tmp;
        }
        return Math.floor( Math.random() * ( max - min + 1 ) + min );
    }

    testHandler = () => {
        this.setState({
            shippings: [...this.state.shippings, this.getReceipt()]
        });
    };

    test2Handler = (id) => {
        console.log("CATCCH: " + id );
    };

    render(){

        let tabs = [
            (<h1 onClick={this.testHandler}>H1</h1>),
            (<h1>H2</h1>)
        ];

        let rows = null;
        if( this.state.shippings.length > 0 ){
            rows = this.state.shippings.map( (shipping,key) => {
                return [
                    (<h1>{shipping.id}</h1>),
                    (<h2 onClick={ ()=>this.test2Handler(shipping.id) } >{shipping.name}</h2>)
                ];
            });
        }

        let headers = [ 'Id', "Numer przesyłki", "ilość", "Cena", "Status", 'Karta' ];

        return (
            <div>
                ShippingList Component
                <Accordion tabs={tabs} >
                    <div onClick={this.testHandler}>
                        <button className="btn--primary">Dodaj przesyłke</button>
                    </div>
                    <div>
                        <Table rows={rows} headers={headers} />
                    </div>
                </Accordion>
            </div>
        );

    };

}

export default ShippingList;
