import React from 'react';
import axios from 'axios';
import CartItem from './CartItem';
import './Cart.scss';

class Cart extends React.Component{

    state = {
        categoryName: null,
        categoryIcon: null,
        cartPrice: null,
        fold: false
    };

    toggleFold = () => {
        this.setState({
            fold: !this.state.fold
        });
    }

    removeItemHandler = ( id ) => {
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','remove_cart_item');
        fd.append('item_id',id);
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                if( response.data.frse_code == 'remove_cart_item_success' ){
                    window.location = window.location;
                }
            })
            .catch( (error) => {
                console.warn(error);
            })
    }

    clearCartHandler = ( ) => {
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','clear_cart');
        fd.append('category_id',this.props.cart.category.id);
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                if( response.data.frse_code == 'clear_cart_success' ){
                    window.location = window.location;
                }
            })
            .catch( (error) => {
                console.warn(error);
            })
    }

    orderCartHandler = ( ) => {
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','order_cart');
        fd.append('category_id',this.props.cart.category.id);
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                if( response.data.frse_code == 'order_cart_success' ){
                    window.location = window.location;
                }
            })
            .catch( (error) => {
                console.warn(error);
            })
    }

    render(){

        let dateCart = new Date();
        let cartPrice = 0;
        let items = this.props.cart.items.map( (item,key) => {
            let itemDate = new Date(item.date);
            if( itemDate.getTime() < dateCart.getTime() ){
                dateCart = itemDate;
            }
            cartPrice += parseInt(item.total_price);
            return <CartItem key={key} item={item} onremove={this.removeItemHandler} />
        } );
        let dateCartString = dateCart.getFullYear()+'/'+(dateCart.getMonth()+1)+'/'+dateCart.getDate();

        return (
            <div className="cart" >

                <div className={"cart__body accordion " + 'is-fold-'+this.state.fold  }>
                    <div className="accordion__header" onClick={this.toggleFold} >
                        <div className="accordion__tab">
                            <span className="font--accordion--header--order"></span>Zamówienie {dateCartString}
                        </div>
                        <div className="accordion__tab">
                        <span className="font--accordion--header--category--name"></span>{this.props.cart.category.name}
                        </div>
                    </div>
                    <div className="accordion__primary ">
                        <div className="table" >
                            <div className="table__header">
                                <div className="table__row">
                                    <div className="table__cell">
                                        <span className="font--table--header"></span>nazwa
                                    </div>
                                    <div className="table__cell">
                                        <span className="font--table--header"></span>ilość
                                    </div>
                                    <div className="table__cell">
                                        <span className="font--table--header"></span>koszt
                                    </div>
                                    <div className="table__cell">
                                        <span className="font--table--header"></span>status wizualizacji
                                    </div>
                                    <div className="table__cell">
                                        <span className="font--table--header"></span>karta produktu
                                    </div>
                                    <div className="table__cell"></div>
                                </div>
                            </div>
                            <div className="table__body">
                                {items}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="cart__footer">
                    <div>
                        <button className="btn--secondary" onClick={this.clearCartHandler}>Wyczyść koszyk</button>
                    </div>
                    <div>
                        <div className="cart__price" >
                            <span className="cart__price-label">łącznie</span>
                            <div className="cart__price-block">
                                <span className="cart__price-value">{cartPrice}</span>
                                <span className="cart__price-currency">PLN</span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <button className="btn--primary" onClick={this.orderCartHandler}>Złóż zamówienie</button>
                    </div>
                </div>
            </div>
        );

    };

}

export default Cart;
