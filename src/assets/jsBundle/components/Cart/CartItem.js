import React from 'react';
import axios from 'axios';


class CartItem extends React.Component{

    btnDeleteHandler = () => {
       this.props.onremove( this.props.item.id );
    }

    render(){

        if( !this.props.item ){
            return (<div><p>Nie można wczytać danych elementu.</p></div>);
        }

        let item = this.props.item;

        let status = 'Oczekiwanie';
        if( item.projects_count > 1 ){
            status = 'Dodano wizualizacje';
        }else if( item.projects_count == 1 ){
            status = 'Dodano wizualizację';
        }

        return (
            <div className="table__row" >
                <div className="table__cell" >
                    {item.title}
                </div>
                <div className="table__cell">
                    {item.count} {item.price_for}
                </div>
                <div className="table__cell">
                    {item.total_price} PLN
                </div>
                <div className="table__cell">
                    {status}
                </div>
                <div className="table__cell">
                    <a href={item.link} >podgląd</a>
                </div>
                <div className="table__cell">
                    <button onClick={this.btnDeleteHandler}>Usuń</button>
                </div>
            </div>
        );

    };

}

export default CartItem;
