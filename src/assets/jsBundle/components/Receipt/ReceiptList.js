import React from 'react';
import axios from '../../elements/axiosInstance';
import Results from '../Results/Results';

class ReceiptList extends Results{

    state = {
        lists: [],
        listsRequestError: false,
        action: 'order_lists'
    };

    dataTableHeaders = [ 'col1d', 'col2d', 'col3', 'col4' ];

}

export default ReceiptList;
