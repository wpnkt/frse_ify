import React from 'react';
const axios = require('axios');

class InvoiceList extends React.Component{

    componentDidMount(){
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','invoices');
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                let data = response.data;
                console.log( data );
            })
            .catch( (error) => {
                console.log(error);
            })
    }

    render(){

        return (
            <div>
                <h1>INVOICE LIST COMPONENT</h1>
            </div>
        );

    }

}

export default InvoiceList;
