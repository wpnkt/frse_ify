import React from 'react';
import ReactDOM from 'react-dom';

class Table extends React.Component{

    render(){

        let headers = null;
        if( this.props.headers ){
            headers = this.props.headers.map( (header,key) => {
                return (<div key={key} className="table__cell">
                    {header}
                </div>);
            });
        }

        let rows = null;
        if( this.props.rows ){
            rows = this.props.rows.map( (row,key) => {
                let cells = row.map( (cell, key) => {
                    return (<div className="table__cell" key={key}>{cell}</div>);
                });
                return <div key={key} className="table__row">{cells}</div>;
            });
        }

        return (<div className="table" >
            <div className="table__header">
                <div className="table__row">
                    {headers}
                </div>
            </div>
            <div className="table__body">
                {rows}
            </div>
        </div>);

    }

}

export default Table;
