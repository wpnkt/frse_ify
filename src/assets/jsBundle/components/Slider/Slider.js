import React from 'react';

class Slider extends React.Component{

    clickImageHandler( img ){
        this.props.changeHandler(img);
    }

    render(){

        let items = this.props.items.map( (img, key) => {
            return (
                <div
                    key={key}
                    className="slider__item"
                    onClick={ ()=>this.clickImageHandler(img) } >
                    <img src={ img } />
                </div>
            );
        });

        return (
            <div className="slider">
                <div className="slider__items">
                    { items }
                </div>
            </div>
        );

    };

}

export default Slider;
