import React from 'react';
import ReactDOM from 'react-dom';
import './Dropdown.scss';

class Dropdown extends React.Component{

    state = {
        isOpen: false,
        classSuffix: '',
    };

    optionClickHandle = ( value ) => {
        this.props.onchange( value );
        this.setState({
            isOpen: false
        });
    };

    valueClickHandler = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    };

    closeOptions = () => {
        this.setState({
            isOpen: false
        });
    }

    handleClickOutside = (event) => {
        const domNode = ReactDOM.findDOMNode(this);
        if (!domNode || !domNode.contains(event.target)) {
            this.closeOptions();
        }
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
    }

    buildSingleOption = ( item, key ) => {
        return (<div className="dropdown__option" key={key} onClick={()=>this.optionClickHandle(item.value)} >
            <label className="dropdown__label">ww{item.content}qq</label>
        </div>);
    };

    buildCurrent = ( item = null ) => {
        if( item === null ){
            return (<h3 className="dropdown__placeholder" >Wybierz...</h3>);
        }else{
            return (<h3 className="dropdown__value" >{this.props.current}</h3>);
        }
    };

    checkItemIsCurrent = ( item ) => {
        if( item.value == this.props.current ){
            return true;
        }
    };

    render(){

        let currentElement = this.buildCurrent();

        let optionsElements = this.props.items.map( (item, key) => {
            if( this.checkItemIsCurrent(item) ){
                currentElement = this.buildCurrent(item);
            }
            return this.buildSingleOption( item, key );
        });

        return (<div className={"dropdown"+this.state.classSuffix} data-open={this.state.isOpen} >
            <div className="dropdown__btn" onClick={()=>this.valueClickHandler()}>
               { currentElement }
               <div className="dropdown__caret"></div>
            </div>
            <div className="dropdown__options" >
                <div className="dropdown__list">
                    { optionsElements }
                </div>
            </div>
        </div>);

    }

}

export default Dropdown;
