import React from 'react';
import './Filedroparea.scss';

class Filedroparea extends React.Component{

    state = {
        classSuffix: ''
    };

    dragEnterHandler = ( ev ) => {
        /*if( this.state.classSuffix === '' ){
            this.setState({
                classSuffix: 'onEnter'
            });
        }*/
        ev.preventDefault();
    };

    dragOverHandler = ( ev ) => {
        /*if( this.state.classSuffix ){
            this.setState({
                classSuffix: ''
            });
        }*/
        ev.preventDefault();
    };

    dropHandler = ( ev ) => {
        ev.preventDefault();
        if (ev.dataTransfer.items) {
            for (var i = 0; i < ev.dataTransfer.items.length; i++) {
                if (ev.dataTransfer.items[i].kind === 'file') {
                    var file = ev.dataTransfer.items[i].getAsFile();
                    this.singleFileHandler( ev.dataTransfer.items[i].getAsFile() );
                    //console.log('... file[' + i + '].name = ' + file.name);
                }
            }
        } else {
            for (var i = 0; i < ev.dataTransfer.files.length; i++) {
                this.singleFileHandler( ev.dataTransfer.files[i] );
                //console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
            }
        }
    };

    inputHandler = ( e ) => {
        this.props.onHandleFile( e.target.files[0] );
    };

    singleFileHandler = ( file ) => {
        this.props.onHandleFile( file );
    };

    render(){

        return (
            <div className={"filedroparea " + this.state.classSuffix}>
                <div className="filedroparea__area"
                    onDragEnter={this.dragEnterHandler}
                    onDragOver={this.dragOverHandler}
                    onDrop={this.dropHandler}
                >
                    <div className="filedroparea__placeholder">
                        <div className="filedroparea__image">
                            <img className="filedroparea__img" src={process.env.IMG_DIR + '/file.png'} alt="Image" />
                        </div>
                        <div className="filedroparea__description">
                            <p>
                                Prosimy o umieszczenie projektu, poprzez button bądź przeciągnięcie
                                plików bezpośrednio do przeglądarki internetowej.
                            </p>
                        </div>
                        <div className="filedroparea__button">
                            <div className="btn--primary">
                                <input type='file' className="filedroparea__input" onChange={this.inputHandler} />
                                <span className="btn__icon">+</span>
                                <span className="btn__text">Projekt oznakowania</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

    }

}

export default Filedroparea;
