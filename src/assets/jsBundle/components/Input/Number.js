import React from 'react';
import './Input.scss';

class InputNumber extends React.Component{

    state = {
        value: null
    }

    changeHandler = ( e ) => {
        let value = e.target.value;
        this.setState({
            value: value
        });
        if( this.props.onchange ){
            this.props.onchange( value );
        }
    }

    render(){

        let placeholder = this.props.placeholder || null;

        return (
            <div className="input--number">
                <input
                    type="number"
                    className="input__input"
                    placeholder={ placeholder }
                    onChange={ this.changeHandler }
                />
            </div>
        );

    }

}

export default InputNumber;
