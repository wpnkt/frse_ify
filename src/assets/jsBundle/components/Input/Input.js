import React from 'react';

class Input extends React.Component{

    state = {
        value: null
    }

    changeHandler = ( value ) => {
        this.setState({
            value: value
        });
        if( this.props.onchange ){
            this.props.onchange( value );
        }
    }

    render(){

        return (
            <div>
                <h1> Input </h1>
            </div>
        );

    }

}

export default Input;
