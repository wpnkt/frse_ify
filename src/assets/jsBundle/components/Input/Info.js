import React from 'react';
import './Input.scss';

class InputInfo extends React.Component{

    render(){

        return (
            <div className="input--info">
                <span className="input__value">{this.props.value}</span>
            </div>
        );

    }

}

export default InputInfo;
