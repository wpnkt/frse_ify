import React from 'react';
import Dropdown from '../Dropdown/Dropdown';
import Colordropdown from '../Colordropdown/Colordropdown';

class InputColorSelect extends React.Component{

    state = {
        //value: this.props.value,
        isOpen: false
    }

    changeHandler = ( value ) => {
        this.setState({
            //value: value,
            isOpen: false
        });
        if( this.props.onchange ){
            this.props.onchange( value );
        }
    }

    openOptions = () => {
        this.setState({
            isOpen: true
        });
    }

    render(){

        let options = this.props.options.map( (color, key) => {
            return (<div className="input__item" key={key} onClick={ ()=>this.changeHandler(color) } >
                    <div className="input__item-color" style={{backgroundColor: color}} >{color}</div>
                </div>);
        } );

        return (
            <div className="input--color-select">
                <Colordropdown items={[{value:'asd',content:"ZXC"},{value:'qqd',content:"AAZXC"}]} />
                <div className="input__value" onClick={this.openOptions} > VALUE: { this.props.value } </div>
                <div className="input__options" data-open={ this.state.isOpen } >
                    { options }
                </div>
            </div>
        );

    }

}

export default InputColorSelect;
