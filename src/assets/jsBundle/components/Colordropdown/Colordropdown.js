import React from 'react';
import ReactDOM from 'react-dom';
import Dropdown from '../Dropdown/Dropdown';

class Colordropdown extends Dropdown{

    state = {
        classSuffix: '--color'
    };

    buildCurrent = ( item = null ) => {
        if( item === null ){
            return (<h3 className="dropdown__placeholder" >Wybierz...</h3>);
        }else{
            let style = {
                backgroundColor: this.props.current,
                width: '34px',
                height: '34px'
            };
            return (<h3 className="dropdown__value" style={style} ></h3>);
        }
    };

    checkItemIsCurrent = ( item ) => {
        if( item == this.props.current ){
            return true;
        }
    };

    buildSingleOption = ( item, key ) => {
        let style = {
            backgroundColor: item,
            width: '34px',
            height: '34px'
        };
        return (<div className="dropdown__option" key={key} onClick={()=>this.optionClickHandle(item)} >
            <label className="dropdown__label" style={style}></label>
        </div>);
    };

}

export default Colordropdown;
