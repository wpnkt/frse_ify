import React from 'react';
import axios from '../../elements/axiosInstance';

import Accordion from '../Accordion/Accordion';
import Table from '../Table/Table';

class Results extends React.Component{

    state = {
        lists: [],
        listsRequestError: false
    };

    action = 'example_action';

    componentDidMount(){
        this.getResults();
    }

    getResults(){
        let fd = new FormData();
        fd.append('action',this.action);
        this.setState({
            listsRequestError: false,
        });
        axios.post(null, fd)
            .then( (response) => {
                this.setState({
                    lists: response.data
                });
            })
            .catch( (error) => {
                console.warn(error);
                this.setState({
                    listsRequestError: true,
                });
            })
    }

    renderEmpty = () => {
        return (<h1>Pusto</h1>);
    }

    renderFail = () => {
        return (<h1>Błąd</h1>);
    }

    action1 = (id) => {
        console.log( "Action 1: " + id );
    };

    //static data table headers
    dataTableHeaders = [ 'col1d', 'col2d'];

    //cell item cinstructors
    cellConstructors = {
        1: (item) => {
            return (<h1>Cell id {item.id}</h1>);
        },
        2: (item) => {
            return (<button onClick={()=>this.action1(item.id)}>Btn</button>);
        }
    };

    //tabs constructors
    tabsConstructors = {
        1: (data) => {
            return 'Tab1';
        },
        2: (data) => {
            return (<h1>Tab2</h1>);
        }
    };

    //return tabs header
    returnTabs = (data) => {
        let tabs = [];
        for( let key in this.tabsConstructors ){
            let tab = this.tabsConstructors[key](data);
            tabs.push( tab );
        }
        return tabs;
    }

    //return array of table headers
    returnDataTableHeaders = () => {
        return this.dataTableHeaders;
    };

    //return array of cells for single row
    returnCells = ( item ) => {
        let cells = [];
        for( let i in this.cellConstructors ){
            cells.push( this.cellConstructors[i]( item ) );
        }
        return cells;
    }

    //return array of rows
    returnDataTableRows = (items) => {
        let rows = null;
        if( items.length ){
            rows = items.map( (item, key) => {
                return this.returnCells( item );
            });
        }
        return rows;
    };

    //zwraca tabele
    renderTable = (data) => {
        let headers = this.returnDataTableHeaders();
        let rows = this.returnDataTableRows( data.items );
        return (<Table
            headers={headers}
            rows={rows}
        >
        </Table>);
    }

    //zwraca akordeon
    renderAccordion = (data, key) => {
        let table = this.renderTable( data );
        let tabs = this.returnTabs(data);
        return (<Accordion
            tabs={tabs}
        >
            {table}
        </Accordion>);
    };

    //zwraca element poj listy
    renderList = (list, key) => {
        let accordion = this.renderAccordion( list, key );
        return (
        <div className="results__list" key={key} >
            {accordion}
        </div>);
    };

    //Zwraca element list
    renderLists = () => {
        if( this.state.lists.length == 0 ){
            if( this.state.listsRequestError ){
                return this.renderFail();
            }
            return this.renderEmpty();
        }
        let lists = this.state.lists.map( (list, key) => {
            return this.renderList( list, key );
        });
        return lists;
    };

    //Render
    render(){
        let lists = this.renderLists();
        return (
            <div className="results">
                { lists }
            </div>
        );
    };

}

export default Results;
