import React from 'react';
import Gallery from '../Gallery/Gallery';
import ReactDOM from 'react-dom';
import './Product.scss';
import InputNumber from '../Input/Number';
import InputInfo from '../Input/Info';
import InputColorSelect from '../Input/ColorSelect';
import Colordropdown from '../Colordropdown/Colordropdown';
import Filearea from '../Filearea/Filearea';

var popupS = require('popups');
import 'popups/css/popupS.css';

const axios = require('axios');

class Product extends React.Component{

    state = {
        title: null,
        category: null,
        defaultPrice: null,
        priceFor: null,
        description: null,
        productColors: [],
        label: null,
        id: this.props.id,
        gallery: null,
        thumbnail: null,
        orderColor: null,
        orderCount: false,
        orderProjects: [],
    };

    getPrice(){
        return this.state.defaultPrice;
    }

    changeMainImageHandler( img ){
        this.setState({
            mainImg: img
        });
    }

    uploadFileHandler(){
        this.setState({projects: [...this.state.projects, event.target.value]});
    }

    componentDidMount() {
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','product');
        fd.append('product_id',this.state.id);
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                let data = response.data;
                if( data ){
                    this.setState({
                        title: data.title,
                        category: data.category.name,
                        defaultPrice: data.default_price,
                        priceFor: data.price_for,
                        description: data.description,
                        productColors: data.product_colors,
                        label: data.label,
                        //id: data.id,
                        gallery: data.gallery,
                        thumbnail: data.thumbnail_url,
                        orderColor: data.product_colors[0],
                    });
                }
            })
            .catch( (error) => {
                console.warn(error);
            })
    };

    handleColorChange = ( value ) => {
        this.setState({
            orderColor: value
        });
    }

    handleCountChange = ( value ) => {
        this.setState({
            orderCount: value
        });
    }

    fileHandler = ( file ) => {
        let project = {
            name: file.name,
            file: file
        };
        this.setState({
            orderProjects: [ ...this.state.orderProjects, project ]
        });
    }

    submitHandler(){
        let values = {
            id: this.state.id,
            color: this.state.orderColor,
            count: this.state.orderCount,
            projects: this.state.orderProjects,
        }
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','order_product');
        fd.append('product_id',values.id);
        fd.append('order_color',values.color);
        fd.append('order_count',values.count);

        let projects = [...this.state.orderProjects];
        for( let i=0, count=projects.length; i<count; i++ ){
            fd.append('file'+i, projects[i].file, projects[i].name );
        }

        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                let data = response.data;
                if( data.success ){
                    popupS.alert({
                        content:     '<b>Dodano produkt do koszyka. Z Twojego budżetu pobraliśmy '+data.totalPrice+' PLN.</b>',
                        labelOk:     'Ok',
                        onSubmit: function() {
                            window.location = window.location;
                        },
                    });
                }else{
                    let err = data.data.errorMessage || 'Wystąpił błąd.';
                    popupS.alert({
                        content:     '<b>'+err+'</b>',
                        labelOk:     'Ok'
                    });
                }
            })
            .catch( (error) => {
                console.log(error);
            })
    }






    render(){

        let gallery = null;
        if( this.state.gallery ){
            gallery = <Gallery main={this.state.thumbnail} items={this.state.gallery} />;
        }

        let description = null;
        if( this.state.description ){
           description = <div className="product__description" dangerouslySetInnerHTML={{__html: this.state.description}}></div>;
        };

        return (
            <div className="product">
                <div className="product__gallery">
                    { gallery }
                </div>
                <div className="product__details">
                    <div>
                        <p className="product__category">Produkty > {this.state.category}</p>
                    </div>
                    <div>
                        <p className="product__title">{this.state.title}</p>
                    </div>
                    <div>
                        <p className="product__price">
                            <span className="product__price-num">{this.getPrice()} </span>
                            PLN/{this.state.priceFor}
                        </p>
                    </div>
                    {description}
                    <div className="product__form">
                        <div className="product__count">
                            <div className="field">
                                <div className="field__wrap-label">
                                    <label className="field__label">Ilość <small className="field__info">[{this.state.priceFor}]</small> </label>
                                </div>
                                <div className="field__wrap-input">
                                    <InputNumber onchange={ this.handleCountChange } />
                                </div>
                            </div>
                        </div>
                        <div className="product__color">
                            <div className="field">
                                <div className="field__wrap-label">
                                    <label className="field__label">Color: </label>
                                </div>
                                <div className="field__wrap-input">
                                    <Colordropdown current={this.state.orderColor} items={this.state.productColors} onchange={ this.handleColorChange } />
                                </div>
                            </div>
                        </div>
                        <div className="product__label">
                            <div className="field">
                                <div className="field__wrap-label">
                                    <label className="field__label">Oznakowanie </label>
                                </div>
                                <div className="field__wrap-input">
                                    <InputInfo value={this.state.label} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="product__projects">
                        <div className="field">
                            <div className="field__wrap-label">
                                <label className="field__label">Wizualizacje</label>
                            </div>
                            <div className="field__wrap-input">
                                <Filearea projects={[]} onFileHandle={this.fileHandler} />
                            </div>
                        </div>
                    </div>
                    <div className="product__actions">
                        <button className="btn--primary" onClick={()=>this.submitHandler()}>Dodaj do koszyka</button>
                    </div>
                </div>
            </div>
        );

    };









}

export default Product;
