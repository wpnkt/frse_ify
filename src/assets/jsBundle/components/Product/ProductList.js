import React from 'react';
import Filters from '../Filters/FiltersProduct';

class ProductList extends React.Component{

    state = {
        preFilteredItems: this.props.items,
        items: [],
        filterColors: this.props.filterColors,
        checkedColors: []
    };

    checkColorHandler = ( color ) => {
        this.setState({
            checkedColors: [ ...this.state.checkedColors, color ]
        });
    };

    uncheckColorHandler = ( color ) => {
        var array = [...this.state.checkedColors];
        var index = array.indexOf(color)
        if (index !== -1) {
          array.splice(index, 1);
          this.setState({checkedColors: array});
        }
    };

    newCheckedColorList = ( colors ) => {
        this.setState({
            checkedColors: colors
        });
    }

    getProductFilterColors = ( item ) => {
        let colors = [];
        let arr = item.fields.colors;
        for( let i=0, count=arr.length; i<count; i++ ){
            colors.push(arr[i].filter_color);
        }
        return colors;
    }

    productColorFilter = ( item ) => {
        let filterColors = this.state.checkedColors; //colory zaznaczone
        if( !filterColors.length ){ return true; } //jesli brak -uznaj wszystkei za zaznaczone
        let productColors = this.getProductFilterColors( item );
        for( let i=0, count=productColors.length; i<count; i++ ){
            if( filterColors.includes( productColors[i] ) ){
                return true;
            }
        }
        return false;
    }

    productFilter(item){
        return this.productColorFilter( item );
    }

    render(){

        let content = this.state.preFilteredItems.map( (item, key)=>{
            if( this.productFilter(item) ){
                return (<a className="loop--product" href={item.link} key={key}>
                    <div className="loop__thumbnail">
                        <img className="loop__img" src={item.thumbnail} alt="" />
                    </div>
                    <div className="loop__grid">
                        <div className="loop__title">{item.post.post_title}</div>
                        <div className="loop__data">{item.fields.prices[0].price} zl {item.fields.price_for}</div>
                        <div className="loop__actions">Zobacz</div>
                    </div>
                </a>);
            }
        })

        return (
            <div className="archive--products">
                <div className="archive__filters">
                    { <Filters colors={this.state.filterColors} check={this.checkColorHandler} uncheck={this.uncheckColorHandler} /> }
                </div>
                <div className="archive__list">
                    <div className="list--products">
                        { content }
                    </div>
                </div>
            </div>
        );

    }

}

export default ProductList;
