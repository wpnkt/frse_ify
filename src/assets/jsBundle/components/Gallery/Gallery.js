import React from 'react';
import Slider from '../Slider/Slider';

import './Gallery.scss';

class Gallery extends React.Component{

    state = {
        main: this.props.main
    }

    clickImageHandler = ( img ) => {
        this.setState({
            main: img
        });
    }

    componentWillReceiveProps = ( img ) => {
        this.setState({
            main: img.items[0]
        });
    }

    render(){

        return (
            <div className="gallery">
                <div className="gallery__main">
                    <img className="gallery__img--main" src={ this.state.main } />
                </div>
                <div className="gallery__thumbnails">
                    <Slider items={this.props.items} changeHandler={ this.clickImageHandler } />
                </div>
            </div>
        );

    };

}

export default Gallery;
