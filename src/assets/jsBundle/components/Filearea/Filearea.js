import React from 'react';
//import './Filearea.scss';
import Filedroparea from '../Filedroparea/Filedroparea';

class Filearea extends React.Component{

    fileHandler = ( file ) => {
        this.props.onFileHandle( file );
    };

    render(){

        let images = this.props.projects.map( (project, key) => {
            return (<div key={key}>
                <h1>Project</h1>
            </div>);
        });

        return (
            <div className="filearea">
                <div>
                    <Filedroparea onHandleFile={this.fileHandler} />
                </div>
                <div>
                    {images}
                </div>
            </div>
        );

    }

}

export default Filearea;
