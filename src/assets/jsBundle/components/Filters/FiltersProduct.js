import React from 'react';

class ColorFilterBtn extends React.Component{
    state = {
        active: false,
        color: this.props.color
    };
    changeHandler(){
        if( this.state.active ){
            this.props.uncheck( this.state.color );
            this.setState({ active: false });
        }else{
            this.props.check( this.state.color );
            this.setState({ active: true });
        }
    }
    render(){
        let style = () => {
            return {
                background: this.state.color
            };
        };
        let activeClass = ''; if(this.state.active){ activeClass = 'is-active'; }
        return (
            <button onClick={()=>this.changeHandler()} className={activeClass + " btn--color"} style={style()} ></button>
        );
    }
}

class FiltersProduct extends React.Component{

    state = {
        colors: this.props.colors,
        checkedColors: []
    };

    render(){
        let btns = this.state.colors.map( (color, key)=>{
            return <ColorFilterBtn color={color} key={key} check={this.props.check} uncheck={this.props.uncheck} />
        })
        return (
            <div className="filters">
                <div className="filters__section">
                    <div className="filters__row"><h1>FILTRY</h1></div>
                </div>
                <div className="filters__section">
                    <div className="grid--colors">
                        { btns }
                    </div>
                </div>
            </div>
        );
    }

}

export default FiltersProduct;
