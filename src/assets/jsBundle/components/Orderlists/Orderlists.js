import React from 'react';
import axios from '../../elements/axiosInstance';
import Results from '../Results/Results';

class OrderLists extends Results{

    state = {
        lists: [],
        listsRequestError: false
    };

    action = 'order_lists';

    dataTableHeaders = [ 'nazwa', 'ilość', 'koszt', 'status wizualizacji', 'karta produktu' ];

    cellConstructors = {
        1: (item) => {
            return (<span>{item.name}</span>);
        },
        2: (item) => {
            return (<span>{item.count}{item.priceFor || null}</span>);
        },
        3: (item) => {
            return (<span>{item.total_price} PLN</span>);
        },
        4: (item) => {
            if( !item.projectsCount ){
                return (<button>Dodaj projekt</button>);
            }
            return (<span>{item.status.name || null}</span>);
        },
        5: (item) => {
            let btnClass = 'btn--secondary';
            (item.link) ? btnClass= "btn--secondary " : btnClass = "btn--secondary--disabled";
            return (<a className={btnClass} href={item.link || null} >
                <span>karta produktu</span>
                <span> > </span>
            </a>);
        }
    };

    tabsConstructors = {
        1: (data) => {
            console.log( data );
            return 'Zamówienie 12/07/2019';
        },
        2: (data) => {
            return 'IZABELA KOWALSKA iza.kowalska@frse.pl';
        },
        3: (data) => {
            return (<h1>data dodania 09-09-2018 || pref. data odb. 09-09-2018</h1>);
        }
    };

}

export default OrderLists;
