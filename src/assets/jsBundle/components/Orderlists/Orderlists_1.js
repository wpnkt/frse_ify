import React from 'react';
const axios = require('axios');

import Results from '../Results/Results';

class OrderLists extends React.Component{

    state = {
        lists: null
    };

    componentDidMount(){
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','order_lists');
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                this.setState({
                    lists: response.data
                });
                console.log( response.data );
            })
            .catch( (error) => {
                console.log(error);
            })
    }

    renderLists = (row) => {
        console.log('renderSingleRow');
    }

    renderList = (row) => {
        console.log('renderSingleRow');
    }

    renderAccordion = (row) => {
        console.log('renderSingleRow');
    }

    renderAccordion = (row) => {
        console.log('renderSingleRow');
    }

    render(){

        let items = null;
        if( this.state.lists ){
            items = this.state.lists.map( (item,key) => {
                return (<div key={key} style={{ padding: '20px', border: '1px solid #333' }}>
                    <h3 > category: {item.category.name}</h3>
                    <h3 > author_mail: {item.author_mail}</h3>
                    <h3 > id: {item.id}</h3>
                </div>);
            });
        }

        return (
            <div>
                <h1> OrderLists COMPONENT</h1>
                <Results
                    renderLists={this.renderLists}
                >

                </Results>
                <div>
                    { items }
                </div>
            </div>
        );

    }

}

export default OrderLists;
