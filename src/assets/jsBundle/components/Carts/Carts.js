import React from 'react';
import axios from 'axios';
import Cart from '../Cart/Cart';

import './Carts.scss';

class Carts extends React.Component{

    state = {
        carts: []
    };

    componentDidMount(){
        let url = process.env.WP_AJAX_URL;
        let fd = new FormData();
        fd.append('action','get_carts');
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': '*/* '
            }
        };
        axios.post(url, fd, config)
            .then( (response) => {
                let data = response.data;
                if( data ){
                    this.setState({
                        carts: data
                    });
                }
            })
            .catch( (error) => {
                console.log(error);
            })
    }

    render(){

        let carts = this.state.carts.map( (cart,key) => {
            return (<Cart cart={cart} key={key} />);
        });

        return (
            <div className="carts">
                <div>
                    {carts}
                </div>
            </div>
        );

    };

}

export default Carts;
