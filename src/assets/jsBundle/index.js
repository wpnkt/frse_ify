import './elements/categoryProducts';
import './elements/singleProduct';
import './elements/carts';

import './elements/orderLists';
import './elements/shippings';
import './elements/receipts';
import './elements/complaints';
import './elements/invoices';
