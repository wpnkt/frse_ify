import React from 'react';
import ReactDOM from 'react-dom';
import InvoiceList from '../components/Invoice/InvoiceList';

function invoicesSingleStart( element ){
    ReactDOM.render( <InvoiceList />, element );
}

function invoices(){
    let selector = '#invoices';
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        invoicesSingleStart( elements[0] ); 
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    invoices();
});
