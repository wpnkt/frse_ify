import React from 'react';
import ReactDOM from 'react-dom';
import ProductList from '../components/Product/ProductList';

const axios = require('axios');

function productsListStart( list ){

    let url = process.env.WP_AJAX_URL;
    let fd = new FormData();
    fd.append('action','filter')
    let config = {
        headers: {
            'Content-Type': 'multipart/form-data',
            'Accept': '*/* '
        }
    };

    axios.post(url, fd, config)
    .then(function (response) {
        let filterColors = [
            '#800000',
            '#9a6324',
            '#808000',
            '#3cb44b',
            '#008080',
            '#000075',
            '#4363d8',
            '#46f0f0',
            '#911eb4',
            '#f032e6',
            '#ff0000',
            '#e6194b',
            '#f58231',
            '#ffe119',
            '#bcf60c',
            '#808080',
            '#000000',
            '#ffffff',
        ];
        let items = response.data;
        ReactDOM.render( <ProductList items={response.data} filterColors={filterColors}/>, list );
    })
    .catch(function (error) {
        console.log(error);
    })
    .finally(function () {
    });

}

function productsList(){
    let selector = '#products';
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        productsListStart( elements[0] );
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    productsList();
});
