import React from 'react';
import ReactDOM from 'react-dom';
import Product from '../components/Product/ProductSingle';

function productSingleStart( element ){
    let id = element.dataset.productId;
    ReactDOM.render( <Product id={id} />, element );
}

function productSingle(){
    let selector = '#single-product';
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        productSingleStart( elements[0] ); 
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    productSingle();
});
