import React from 'react';
import ReactDOM from 'react-dom';
import Carts from '../components/Carts/Carts';

function cartsSingleStart( element ){
    ReactDOM.render( <Carts />, element );
}

function carts(){
    let selector = '#carts';
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        cartsSingleStart( elements[0] ); 
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    carts();
});
