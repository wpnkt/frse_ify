import React from 'react';
import ReactDOM from 'react-dom';
import ShippingList from '../components/Shipping/ShippingList';

function shippingsSingleStart( element ){
    ReactDOM.render( <ShippingList />, element );
}

function shippings(){
    let selector = '#shippings';
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        shippingsSingleStart( elements[0] ); 
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    shippings();
});
