import React from 'react';
import ReactDOM from 'react-dom';
import Receipts from '../components/Receipt/ReceiptList';

function receiptsSingleStart( element ){
    ReactDOM.render( <Receipts />, element );
}

function receipts(){
    let selector = '#receipts';
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        receiptsSingleStart( elements[0] ); 
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    receipts();
});
