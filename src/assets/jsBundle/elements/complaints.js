import React from 'react';
import ReactDOM from 'react-dom';
import ComplaintList from '../components/Complaint/ComplaintList';

function complaintsSingleStart( element ){
    ReactDOM.render( <ComplaintList />, element );
}

function complaints(){
    let selector = '#complaints';
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        complaintsSingleStart( elements[0] ); 
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    complaints();
});
