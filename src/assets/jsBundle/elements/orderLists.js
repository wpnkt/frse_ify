import React from 'react';
import ReactDOM from 'react-dom';
import OrderLists from '../components/Orderlists/Orderlists';

function orderListsSingleStart( element ){
    ReactDOM.render( <OrderLists />, element );
}

function orderLists(){
    let selector = '#order_lists';
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        orderListsSingleStart( elements[0] );
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
    orderLists();
});
