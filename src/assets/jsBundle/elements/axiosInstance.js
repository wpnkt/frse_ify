import axios from 'axios';

const instance = axios.create({
    baseURL: process.env.WP_AJAX_URL,
    timeout: 5000,
    headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': '*/*'
    }
});

export default instance;
