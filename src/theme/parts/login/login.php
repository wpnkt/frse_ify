<div class="form-login">

<?php if( $_GET['login'] == 'failed' ){ ?>
       
    <div class="alert--wrong">
        <p class="description--alert--wrong">Wprowadziłeś błędne dane.</p>    
    </div>

<?php } ?>

<?php 
    wp_login_form([
        'redirect'       => dashboard_url(),
        'remember'       => false,
        'label_username' => __( 'Użytkownik:' ),
        'label_password' => __( 'Hasło' ),
        'label_remember' => __( 'Zapamiętaj' ),
        'label_log_in'   => __( 'Zaloguj' ),
    ]);
?>


<div>
    <a class="btn--secondary" href="<?php echo home_url().'/wp-login.php?action=lostpassword'; ?>">Nie pamietam hasła</a>
</div>


</div>
