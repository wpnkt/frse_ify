<div class="sidebar">
    <div class="sidebar__top">
        <div class="sidebar__menu">
            <ul class="menu--sidebar">
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_home_url(); ?>" >
                        <span class="btn__icon icon icon--list"></span>
                        <span class="btn__text ">Kategorie produktów</span>
                    </a> 
                    <ul class="menu__sublist">
                        <li class="menu__subitem">
                            <a class="menu__subbtn btn--sidebar--sub" href="<?php echo get_term_link('frse_cat_gadgets', 'frse_category'); ?>" >
                                <span class="btn__icon icon icon--list"></span>
                                <span class="btn__text ">Gadżety</span>
                            </a>
                        </li>
                        <li class="menu__subitem">
                            <a class="menu__subbtn btn--sidebar--sub" href="<?php echo get_term_link('frse_cat_others', 'frse_category'); ?>" >
                                <span class="btn__icon icon icon--list"></span>
                                <span class="btn__text ">Inne</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu__separator"></li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('cart_frse'); ?>" >
                        <span class="btn__icon icon icon--cart"></span>
                        <span class="btn__text ">Mój koszyk</span>
                    </a> 
                </li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('order_list'); ?>" >
                        <span class="btn__icon icon icon--order_list"></span>
                        <span class="btn__text ">Moje zamówienia</span>
                    </a> 
                </li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('shipping'); ?>" >
                        <span class="btn__icon icon icon--shipping"></span>
                        <span class="btn__text ">Przesyłki</span>
                    </a> 
                </li>
                <li class="menu__separator"></li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('complaint'); ?>" >
                        <span class="btn__icon icon icon--alert-danger"></span>
                        <span class="btn__text ">Reklamacje</span>
                    </a> 
                </li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('receipt'); ?>" >
                        <span class="btn__icon icon icon--receipt"></span>
                        <span class="btn__text ">Protokoły odbioru</span>
                    </a> 
                </li>
            </ul>
        </div>
    </div>
    <div class="sidebar__bottom">
        <div class="sidebar__menu">
            <ul clss="menu--sidebar--bottom">
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar--bottom" href="#" >
                        <?php 
                            $active_category = frse_active_category(); 
                        ?>
                        <span class="btn__icon icon icon--database"></span>
                        <div class="btn__text ">
                            <span class="btn__label"><?php echo $active_category['budget']; ?> PLN</span>
                            <span class="btn__sublabel">zostało do wydania</span>
                        </div>
                    </a> 
                </li> 
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar--bottom" href="#" >
                        <span class="btn__icon icon icon--cart-star"></span>
                        <div class="btn__text ">
                            <span class="btn__label">Kategoria</span>
                            <span class="btn__sublabel"><?php echo $active_category['term']->name; ?></span>
                        </div>
                    </a> 
                </li>
            </ul>
        </div>
    </div>
</div> 


