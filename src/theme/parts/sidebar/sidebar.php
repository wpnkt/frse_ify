<div class="sidebar">
    <div class="sidebar__top">
        <div class="sidebar__menu">
            <ul clss="menu--sidebar">
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar">
                        <span class="btn__icon icon--list"></span>
                        <span class="btn__icon "></span>
                        <i class="fa fa-address-card-o" aria-hidden="true"></i>

                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="sidebar__bottom"></div>
</div> 
