<div class="sidebar">
    <div class="sidebar__top">
        <div class="sidebar__menu">
            <ul class="menu--sidebar">
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('order_list'); ?>" >
                        <span class="btn__icon icon--order_list"></span>
                        <span class="btn__text ">Zamówienia</span>
                    </a> 
                </li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('shipping'); ?>" >
                        <span class="btn__icon icon icon--shipping"></span>
                        <span class="btn__text ">Przesyłki</span>
                    </a> 
                </li>
                <li class="menu__separator"></li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('complaint'); ?>" >
                        <span class="btn__icon icon icon--alert-danger"></span>
                        <span class="btn__text ">Reklamacje</span>
                    </a> 
                </li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('receipt'); ?>" >
                        <span class="btn__icon icon icon--receipt"></span>
                        <span class="btn__text ">Protokoły odbioru</span>
                    </a> 
                </li>
                <li class="menu__item">
                    <a class="menu__btn btn--sidebar" href="<?php echo get_post_type_archive_link('invoice'); ?>" >
                        <span class="btn__icon icon--list"></span>
                        <span class="btn__text ">Faktury</span>
                    </a> 
                </li>
            </ul>
        </div>
    </div>
</div> 
