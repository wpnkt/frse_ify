<div class="grid--categories">
<?php 
    $categories = frse_current_user_categories();
    foreach( $categories as $category ){
        global $loop_category;
        $loop_category = $category;
        loop('category');
    }
?>
</div>
