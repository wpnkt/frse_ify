<?php 
    $user = wp_get_current_user();
?>


<div class="topbar">
    <div class="container">
        <div class="grid--primary">
            <div class="grid__item--sidebar">
                <div class="brand">
                    <img class="brand__logo--top" src="<?php echo get_template_directory_uri() . '/img/frse_logo.jpg'; ?>" alt="Logo FRSE" />
                </div>
            </div>
            
            <?php if( is_user_logged_in() ){ ?>
                <div class="grid__item--main grid--between">
                    <div class="grid__item">
                        <p class="description--topbar">
                            Witaj, jesteś zalogowany, jako <span class="description__strong"><?php echo $user->data->user_email; ?></span>
                        </p>
                    </div>
                    <div class="grid__item">
                        <a class="btn--logout" href="<?php echo wp_logout_url( get_home_url() ); ?>">
                            <span class="btn__icon icon icon--logout"></span>
                            <span class="btn__text">Wyloguj</span>
                        </a>
                    </div>
                </div>
            <?php } ?>
            
        </div>
    </div>
</div>
