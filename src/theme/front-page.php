<?php get_header(); ?>

<?php 
    $role = frse_user_role();
    switch( $role ){
        case "administrator":
            view('dashboard');
            break;
        case "frse_admin":
            wp_redirect( home_url( '/wp-admin/' ) );
            exit();
            break;
        case "frse_supervisor":
            view('categories');
            break;
        case "frse_customer_in":
            view('categories');
            break;
        case "frse_producer":
            view('order_lists');
            break;
        default:
            view('login');
            exit();
    } 
?>

<?php get_footer(); ?>
