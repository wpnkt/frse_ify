<div class="primary">

    <div class="primary__sidebar">
        <?php part('sidebar'); ?> 
    </div>
    
    <div class="primary__main">
        <div class="main">
            <?php single('product'); ?>
        </div>
    </div>

</div>
