<div class="primary">
    
    <div class="primary__sidebar">
        <?php part('sidebar'); ?> 
    </div>
    
    <div class="primary__main">
        <div class="main">
            
            <div class="content">
                <div class="row">
                    <h1 class="title--view">
                        <span class="title__icon icon icon--list"></span>
                        <span class="title__text"> Receipts view</span>
                    </h1>
                </div>
                <div class="row">
                    <div id="receipts"></div>
                </div>
            </div>


        </div>
    </div>

</div>
