<?php 

// Information needed for creating the plugin's pages
$page_definitions = array(
    'member-login' => array(
        'title' => __( 'Sign In', 'personalize-login' ),
        'content' => '[custom-login-form]'
    ),
    'member-account' => array(
        'title' => __( 'Your Account', 'personalize-login' ),
        'content' => '[account-info]'
    ),
    'member-register' => array(
        'title' => __( 'Register', 'personalize-login' ),
        'content' => '[custom-register-form]'
    ),
    'member-password-lost' => array(
        'title' => __( 'Forgot Your Password?', 'personalize-login' ),
        'content' => '[custom-password-lost-form]'
    ),
    'member-password-reset' => array(
        'title' => __( 'Pick a New Password', 'personalize-login' ),
        'content' => '[custom-password-reset-form]'
    )
);

/**
 * Redirects the user to the custom "Forgot your password?" page instead of
 * wp-login.php?action=lostpassword.
 */
/*function redirect_to_custom_lostpassword() {
    if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
        if ( is_user_logged_in() ) {
            $this->redirect_logged_in_user();
            exit;
        }
 
        wp_redirect( home_url( 'member-password-lost' ) );
        exit;
    }
}*/
//add_action( 'login_form_lostpassword', array( $this, 'redirect_to_custom_lostpassword' ) );




/**
 * A shortcode for rendering the form used to initiate the password reset.
 *
 * @param  array   $attributes  Shortcode attributes.
 * @param  string  $content     The text content for shortcode. Not used.
 *
 * @return string  The shortcode output
 *//*
public function render_password_lost_form( $attributes, $content = null ) {
    // Parse shortcode attributes
    $default_attributes = array( 'show_title' => false );
    $attributes = shortcode_atts( $default_attributes, $attributes );
 
    if ( is_user_logged_in() ) {
        return __( 'You are already signed in.', 'personalize-login' );
    } else {
        return $this->get_template_html( 'password_lost_form', $attributes );
    }
}
add_shortcode( 'custom-password-lost-form', array( $this, 'render_password_lost_form' ) );

*/