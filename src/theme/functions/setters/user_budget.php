<?php

    function frse_put_budget( $user_id, $category_id, $new_budget ){
        $categories = get_field( 'categories', 'user_'.$user_id );
        foreach($categories as $key => $cat ){
            if( $cat['category'] == $category_id ){
                $value = [
                    'category' => $category_id,
                    'budget' => $new_budget
                ];
                $num = $key+1;
                return $update_status = update_row( 'categories', $num, $value, 'user_'.$user_id );
            }
        }
        return false;
    }

    function frse_update_budget( $user_id, $category_id, $price_pln ){

    }

    function frse_add_budget( $user_id, $category_id, $price_pln ){
        $user_current_budget = frse_get_user_budget_by_category_id($category_id);
        $new_budget = $user_current_budget + $price_pln;
        return frse_put_budget( $user_id, $category_id, $new_budget );
    }

    function frse_subtract_budget( $user_id, $category_id, $price_pln ){

    }

    function frse_update_current_user_budget( $category_id, $price ){


    }

    function frse_add_budget_to_current_user( $category_id, $price ){
        $user_id = get_current_user_id();
        return frse_add_budget($user_id, $category_id, $price);
    }

    function frse_remove_budet_from_current_user( $category_id, $price ){


    }
