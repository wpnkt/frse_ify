<?php

    function cart_item_remove_from_cart( $cart_item_id ){
        $post = get_post( $cart_item_id );
        $curr_user_id = get_current_user_id();
        if( $post->post_author != $curr_user_id ){ return false; }

        $category = frse_get_category_by_post_id( $cart_item_id );
        $price = get_field('total_price', $cart_item_id);

        $add_budget_status = frse_add_budget_to_current_user( $category->term_id, $price );
        if( $add_budget_status ){
            $delete =  wp_delete_post( $cart_item_id );
            if( $delete ){
                return true;
            }
        }
    }

    function frse_cart_clear( $category_id ){
        $items = frse_get_cart_products_by_category_id( $category_id );
        $price_total = 0;
        $user_id = get_current_user_id();
        foreach( $items as $item ){
            if( $item->post_author != $user_id ){ return false; }
            $field = get_field('total_price', $item->ID);
            $price_total += $field;
        }
        $add_budget_status = frse_add_budget_to_current_user( $category_id ,$price_total );
        $delete_items_status = true;
        if( $add_budget_status ){
            foreach( $items as $item ){
                wp_delete_post( $item->ID );
            }
            return true;
        }
        return false;
    }

    function frse_cart_order( $category_id ){
        $items = frse_get_cart_products_by_category_id( $category_id );
        if( count($items) == 0 ){ return false; }
        $price_total = 0;
        $user_id = get_current_user_id();
        foreach( $items as $item ){
            if( $item->post_author != $user_id ){ return false; }
        }
        $params = [
            'post_type' => 'order_list'
        ];
        $order_id = wp_insert_post( $params );
        if( $order_id ){
            wp_set_post_terms( $order_id, $category_id, 'frse_category', false );
            wp_publish_post( $order_id );

        }else{ return false; }
        foreach( $items as $item ){
            set_post_type( $item->ID, 'order_item' );
            update_field( 'order_id', $order_id, $item->ID );
        }
        return true;
    }


