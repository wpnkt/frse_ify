<?php 

//ONCHANGE
/*function frse_category_onchange(){
    echo '<h1>Change category</h1>';
}*/
 

//CREATE 
add_action( 'init', 'create_taxonomy_frse_category', 0 );
function create_taxonomy_frse_category() { 
    $labels = array(
        'name' => _x( 'Kategoria FRSE', 'taxonomy general name' ),
        'singular_name' => _x( 'Kategoria FRSE', 'taxonomy singular name' ),
        'search_items' =>  __( 'Kategoria FRSE' ),
        'popular_items' => __( 'Popularne Kategorie' ),
        'all_items' => __( 'Wszystkie Kategorie' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Kategoria' ), 
        'update_item' => __( 'Update Kategoria' ),
        'add_new_item' => __( 'Add New Kategoria' ),
        'new_item_name' => __( 'New Kategoria Name' ),
        'separate_items_with_commas' => __( 'Separate Kategoria with commas' ),
        'add_or_remove_items' => __( 'Add or remove Kategoria' ),
        'choose_from_most_used' => __( 'Choose from the most used Kategoria' ),
        'menu_name' => __( 'Kategoria' ),
    ); 
    register_taxonomy('frse_category','product',array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_quick_edit' => true,
        //'update_count_callback' => 'frse_category_onchange',
        'query_var' => true,
        'capabilities' => [
            'manage_terms' => 'edit_posts',
            'edit_terms' => 'administrator',
            'delete_terms' => 'administrator',
            'assign_terms' => 'edit_posts'
        ],
        'rewrite' => array( 'slug' => 'frse_category' ),
		//'meta_box_cb'       => 'frse_category_meta_box',
    ));

}


/**
 * Display Movie Rating meta box 
 */
/*
function status_invoice_meta_box( $post ) {
	$terms = get_terms( 'frse_category', array( 'hide_empty' => false ) );
	$post  = get_post();
	$rating = wp_get_object_terms( $post->ID, 'frse_category', array( 'orderby' => 'term_id', 'order' => 'ASC' ) );
	$name  = '';
    if ( ! is_wp_error( $rating ) ) {
    	if ( isset( $rating[0] ) && isset( $rating[0]->name ) ) {
			$name = $rating[0]->name;
	    }
    }
	foreach ( $terms as $term ) {
    ?>
		<label title='<?php esc_attr_e( $term->name ); ?>'>
		    <input type="radio" name="frse_category" value="<?php esc_attr_e( $term->name ); ?>" <?php checked( $term->name, $name ); ?>>
			<span><?php esc_html_e( $term->name ); ?></span>
		</label><br>
    <?php
    }
}
*/



/**
 * Save the movie meta box results.
 *
 * @param int $post_id The ID of the post that's being saved.
 */
/*function save_frse_category_meta_box( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	if ( ! isset( $_POST['frse_category'] ) ) {
		return;
	}
	$rating = sanitize_text_field( $_POST['frse_category'] );
	
	// A valid rating is required, so don't let this get published without one
	if ( empty( $rating ) ) {
		// unhook this function so it doesn't loop infinitely
		remove_action( 'save_post_invoice', 'save_frse_category_meta_box' );
		$postdata = array(
			'ID'          => $post_id,
			'post_status' => 'draft',
		);
		wp_update_post( $postdata );
	} else {
		$term = get_term_by( 'name', $rating, 'frse_category' );
		if ( ! empty( $term ) && ! is_wp_error( $term ) ) {
			wp_set_object_terms( $post_id, $term->term_id, 'frse_category', false );
		}
	}
}
add_action( 'save_post_invoice', 'save_frse_category_meta_box' );
*/

/*
add_action( 'save_post_invoice', 'set_post_default_invoice_status', 10,3 );
function set_post_default_invoice_status( $post_id, $post, $update ) {

    // Only want to set if this is a new post!
    if ( $update ){
        return;
    }
     
    // Only set for post_type = post!
    if ( 'invoice' !== $post->post_type ) {
        return;
    }
     
    // Get the default term using the slug, its more portable!
    $term = get_term_by( 'slug', 'invoice_send', 'status_invoice' );
 
    wp_set_post_terms( $post_id, $term->term_id, 'status_invoice', true );

}
*/


