<?php 

    function frse_user_has_any_role( $roles ){ 
        $user_roles = frse_user_roles();
        foreach( $roles as $role ){
            if (in_array( $role, $user_roles)){
                return true;
            } 
        }
        return false;
    }

    function frse_user_has_role( $role ){
        $roles = frse_user_roles();
        if (in_array( $role, $roles)){
            return true;
        } 
        return false;
    }

    function frse_roles(){
        return [
            'frse_customer_in', 'frse_producer', 'frse_supervisor', 'frse_admin'
        ];
    }

    function frse_is_user_customer(){
        return frse_user_has_role( 'frse_customer_in' );
    }

    function frse_is_user_producer(){
        return frse_user_has_role( 'frse_producer' );
    }

    function frse_is_user_supervisor(){
        return frse_user_has_role( 'frse_supervisor' );
    }

    function frse_is_user_admin(){
        return frse_user_has_role( 'frse_admin' );
    }

    function frse_current_user_role(){
        return frse_user_role();
    }

    function frse_user_role(){
        $roles = frse_user_roles();
        if( count($roles) > 0 ){
            return $roles[0];
        }
        return false;
    }

    function frse_user_roles(){
        if( is_user_logged_in() ) {
        $user = wp_get_current_user();
        $roles = ( array ) $user->roles;
            return $roles;
        } else {
            return array();
        }
    }

    function frse_get_user_categories_obj(){
        //$ret = [];
        //$profiles = frse_get_user_profiles();
        //return $ret;
    }

    function get_icon_name_by_category( $category ){
        return 'example-icon-name';
    }

    function frse_get_user_profiles(){
        $ret = [];
        $posts = get_posts(array(
            'numberposts'	=> -1,
            'post_type'		=> 'user_profile',
            'meta_query'	=> array(
                array(
                    'key'	 	=> 'user',
                    'value'	  	=> get_current_user_id(),
                    'compare' 	=> '=',
                ),
            ),
        ));
        foreach($posts as $post){
            $fields = get_fields($post->ID);
            $ret[] = $fields;
        }
        return $ret;
    }

    function frse_get_categories_for_sidebar_items(){
        $ret = [];
        $profiles = frse_get_user_profiles();
        foreach($profiles as $profile){
            $profile['icon'] = get_icon_name_by_category( $profile['category']->slug );
            $ret[] = $profile;
        }
        return $ret;
    }

    function frse_get_sidebar_menu_items( ){
        $items = [];
        $role = frse_user_role();

        //Kategorie
        $items[] = [
            'icon' => 'icon--categories',
            'name' => 'Kategorie produktów',
            'link' => get_post_type_archive_link('frse_category'),
            'subitems' => frse_get_categories_for_sidebar_items()
        ];

        if( frse_user_has_any_role( ['frse_admin','frse_customer_in'] ) ){
            
        }
        

        return $items;
    }

    function frse_view_role(  ){
        $role = frse_user_role();
        if ( is_user_logged_in() ) {
            switch( $role ){
                case "administrator":
                    return 'administrator';
                    break;
                case "frse_admin":
                    return 'frse_admin';
                    break;
                case "frse_supervisor":
                    return 'frse_supervisor';
                    break;
                case "frse_customer":
                    return 'frse_customer';
                    break;
                case "frse_customer_in":
                    return 'frse_customer';
                    break;
                case "frse_producer":
                    return 'frse_producer';
                    break;
                default:
                    return 'other';
            } 
        }else {
            return 'unlogged';
        }
    }

    function frse_logged(){
        if( is_user_logged_in() ){
            if (in_array( frse_user_role(), ['frse_admin','frse_supervisor','frse_customer','frse_producer'])){
                return true;
            } 
        }
        return false;
    }

    function frse_set_current_category( $term_id ){
        $x = update_field( 'current_category', $term_id, 'user_'.get_current_user_id() );
    }

    function frse_get_current_category_id(  ){
        return get_field( 'current_category', 'user_'.get_current_user_id() );
    }

    function frse_get_products_from_active_directory_by_current_user(){
        $ret = [];
        $products = frse_get_products_by_category_id( frse_get_current_category_id() );
        foreach( $products as $prod ){
            $ret[] = [
                'post' => $prod,
                'fields' => get_fields($prod->ID),
                'link' => get_permalink($prod->ID),
                'thumbnail' => get_the_post_thumbnail_url($prod->ID)
            ];
        }
        return $ret;
    }

    function frse_product_by_id( $prod_id ){
        $categories = get_the_terms( $prod_id, 'frse_category' );
        $ret = [
            'post' => get_post($prod_id),
            'fields' => get_fields($prod_id),
            'link' => get_permalink($prod_id),
            'thumbnail' => get_the_post_thumbnail_url($prod_id),
            'category' => $categories[0],
        ];
        return $ret;
    }

    function frse_active_category(){
        $term_id = get_field( 'current_category', 'user_'.get_current_user_id() );
        $category = frse_single_category($term_id);
        return $category;
    }
