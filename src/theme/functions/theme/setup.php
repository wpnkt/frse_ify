<?php 

    function print_log( $x, $title = false ){
        echo '<div>';
        if( $title ){
            echo '<h2>'.$title.'</h2>';
        }
        echo '<pre>';
        if( is_array($x) || is_object($x) ){
            print_r( $x);
        }else{
            echo $x;
        }
        echo '</pre>';
        echo '</div>';
    }
 
    function view( $name, $view_role = NULL ){
        if( $view_role == NULL ){ 
            $view_role = frse_view_role(); 
            get_template_part( 'views/'.$name.'/'.$name, $view_role ); 
        }else if( $view_role == false ){
            get_template_part( 'views/'.$name.'/'.$name ); 
        }else{
            get_template_part( 'views/'.$name.'/'.$name, $view_role ); 
        }
    }

    function single( $name, $view_role = NULL ){
        if( $view_role == NULL ){ 
            $view_role = frse_view_role(); 
            get_template_part( 'single/'.$name.'/'.$name, $view_role ); 
        }else if( $view_role == false ){
            get_template_part( 'single/'.$name.'/'.$name ); 
        }else{
            get_template_part( 'single/'.$name.'/'.$name, $view_role ); 
        }
    }

    function loop( $name, $view_role = NULL ){
        if( $view_role == NULL ){ 
            $view_role = frse_view_role(); 
            get_template_part( 'loop/'.$name.'/'.$name, $view_role ); 
        }else if( $view_role == false ){
            get_template_part( 'loop/'.$name.'/'.$name ); 
        }else{
            get_template_part( 'loop/'.$name.'/'.$name, $view_role ); 
        }
    }

    function part( $name, $view_role = NULL ){
        if( $view_role === NULL ){ 
            $view_role = frse_view_role(); 
            get_template_part( 'parts/'.$name.'/'.$name, $view_role );
        }else if( $view_role === false ){
            get_template_part( 'parts/'.$name.'/'.$name );
        }else{
            get_template_part( 'parts/'.$name.'/'.$name, $view_role );
        }
    }

    function frse_resources() {
        wp_enqueue_style( 'frse-icons', get_template_directory_uri() . '/icons.css' );
        wp_enqueue_style( 'main-style', get_template_directory_uri() . '/main.css' );
        //wp_enqueue_script( 'header_js', get_template_directory_uri() . '/js/header-bundle.js', null, 1.0, false );
        //wp_enqueue_script( 'footer_js', get_template_directory_uri() . '/js/footer-bundle.js', null, 1.0, true );

        wp_enqueue_script( 'index_js', get_template_directory_uri() . '/index.js', null, 1.0, true );
    }
    add_action( 'wp_enqueue_scripts', 'frse_resources' );

