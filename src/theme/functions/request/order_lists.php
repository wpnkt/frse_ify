<?php

    add_action('wp_ajax_order_lists', 'frse_ajax_order_lists');
    function frse_ajax_order_lists() {
        $order_lists = frse_get_normalize_order_lists();
        echo json_encode($order_lists);
        exit();
    }
