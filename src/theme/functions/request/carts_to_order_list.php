
<?php 

    add_action('wp_ajax_carts_to_order_list', 'frse_request_carts_to_order_list');
    function frse_request_carts_to_order_list() {
        $ret = $_POST['cat_id'];
        $posts = frse_get_cart_products_by_category_id( $_POST['cat_id'] );

        $order_id = wp_insert_post([
            'post_type' => 'order_list'
        ]);
        wp_set_post_terms( $order_id, (int)$_POST['cat_id'], 'frse_category' );
        wp_publish_post($order_id);

        foreach( $posts as $post_cart ){
            update_field( 'order_id', $order_id, $post_cart->ID );
            set_post_type( $post_cart->ID, 'order_item' );
        }
            
        echo wp_send_json( $order_id );
        exit();
    } 
