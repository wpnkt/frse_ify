<?php

    //get all carts for current user
    add_action('wp_ajax_get_carts', 'frse_request_get_carts');
    function frse_request_get_carts() {
        $ret = frse_get_normalize_user_carts();
        echo wp_send_json( $ret );
        exit();
    }

    add_action('wp_ajax_remove_cart_item', 'frse_remove_cart_item');
    function frse_remove_cart_item() {
        if( $_POST["item_id"]  ){
            $remove_success = cart_item_remove_from_cart(  $_POST["item_id"] );
        }
        if( $remove_success ){
            $ret = [
                'message' => 'Udało się usunąć przedmiot z koszyka.',
                'frse_code'  => 'remove_cart_item_success'
            ];
            echo wp_send_json( $ret );
            exit();
        }else{
            $ret = [
                'message' => 'Nie udało się usunąć przedmiot z koszyka.',
                'frse_code'  => 'remove_cart_item_false'
            ];
            echo wp_send_json_error( $ret );
            exit();
        }
        $ret = [
            'message' => 'Nie udało się usunąć przedmiot z koszyka.',
            'frse_code'  => 'remove_cart_item_failed',
            'why' => 'empty_id'
        ];
        echo wp_send_json_error( $ret );
        exit();
    }


    add_action('wp_ajax_clear_cart', 'frse_request_clear_cart');
    function frse_request_clear_cart() {
        if( $_POST["category_id"]  ){
            $remove_success = frse_cart_clear( $_POST["category_id"] );
        }
        if( $remove_success ){
            $ret = [
                'message' => 'Udało się wyczyścic koszyk.',
                'frse_code'  => 'clear_cart_success'
            ];
            echo wp_send_json( $ret );
            exit();
        }else{
            $ret = [
                'message' => 'Nie udało się wyczyścic koszyka.',
                'frse_code'  => 'clear_cart_failed'
            ];
            echo wp_send_json_error( $ret );
            exit();
        }
        $ret = [
            'message' => 'Nie udało się wyczyścic koszyka.',
            'frse_code'  => 'clear_cart_failed',
            'why' => 'empty_id'
        ];
        echo wp_send_json_error( $ret );
        exit();
    }

    add_action('wp_ajax_order_cart', 'frse_request_order_cart');
    function frse_request_order_cart() {
        if( $_POST["category_id"]  ){
            $remove_success = frse_cart_order( $_POST["category_id"] );
        }
        if( $remove_success ){
            $ret = [
                'message' => 'Udało się zamówić koszyk.',
                'frse_code'  => 'order_cart_success'
            ];
            echo wp_send_json( $ret );
            exit();
        }else{
            $ret = [
                'message' => 'Nie udało się zamówić koszyka.',
                'frse_code'  => 'order_cart_failed'
            ];
            echo wp_send_json_error( $ret );
            exit();
        }
        $ret = [
            'message' => 'Nie udało się zamówić koszyka.',
            'frse_code'  => 'order_cart_failed',
            'why' => 'empty_id'
        ];
        echo wp_send_json_error( $ret );
        exit();
    }



