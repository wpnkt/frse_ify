<?php

if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
}

add_action('wp_ajax_upload_test', 'frse_upload_test');
function frse_upload_test() {

    $movefile = frse_upload_file( $_FILES['file'] );
    if ( $movefile && ! isset( $movefile['error'] ) ) {
        echo json_encode($movefile);
    }else{
        echo json_encode($movefile);
    }
    exit();
}

function frse_upload_file( $file, $name = false ){
    $uploadedfile = $file;
    $upload_overrides = array(
        'test_form' => false
    );
    $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
    if ( $movefile && ! isset( $movefile['error'] ) ) {
        return $movefile;
    } else {
        //echo $movefile['error'];
        return $movefile;
    }
}

function frse_upload_media( $file ){
    $movefile = frse_upload_file( $file );
    $type = $movefile['type'];
    $attachment = array(
        'post_title' => '$title',
        'post_type' => 'attachment',
        'post_content' => '$content',
        'post_parent' => '$pid',
        'post_mime_type' => $type,
        'guid' => $file['url'],
    );
    $attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);
    return $attach_id;
}

function frse_upload_project_to_item( $post_id, $file ){
    $url = frse_upload_file( $file );
    $attach_id = frse_upload_media($file);
    $row = array(
        'file'	=> wp_get_attachment_url($attach_id),
        'date'	=> date('m/d/Y h:i:s a', time()),
        'user_id'	=> get_current_user_id()
    );
    $x = add_row( 'projects', $row, $post_id );
    print_log( $x );
    return true;
}
