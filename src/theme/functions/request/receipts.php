<?php 

    add_action('wp_ajax_receipts', 'frse_ajax_receipts');
    function frse_ajax_receipts() { 
        $receipts = frse_get_receipts();
        echo json_encode($receipts);
        exit();
    } 
