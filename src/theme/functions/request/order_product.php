<?php

    add_action('wp_ajax_order_product', 'frse_request_order_product');
    function frse_request_order_product() {
        $product = get_post($_POST['product_id']);
        $product_categories = get_the_terms( $_POST['product_id'], 'frse_category' );
        //sprawdz kategorie po id produktu
        $prod_cat = $product_categories[0];
        //ustal wycene
        $totalPrice = frse_get_product_total_price( $_POST['product_id'], $_POST['order_count'] );
        //sprawdz dostepne srodki
        $userBudget = (int)frse_get_user_budget_by_category_id( $prod_cat->term_id );

        //sprawdz czy user ma wystarczajacy budzet
        if( $totalPrice > $userBudget ){
            echo wp_send_json_error([ 'errorMessage' => 'Nie masz wystarczających środków na koncie.' ]);
            exit();
        }

        //sprawdz czy uzupelniono wszystkie pola, jesli tak - rozpocznij dodawanie do koszyka
        if( $_POST['order_color'] === 'false' ){
            echo wp_send_json_error([ 'errorMessage' => 'Wybierz kolor produktu.' ]);
            exit();
        }else if( $_POST['order_count'] === 'false' ){
            echo wp_send_json_error([ 'errorMessage' => 'Wybierz ilość zamawianego produktu.' ]);
            exit();
        }
        else if( $_POST['order_count'] === 'false' ){
            echo wp_send_json_error([ 'errorMessage' => 'Wybierz ilość zamawianego produktu.' ]);
            exit();
        }

        //Utwórz post order_item
        $arr = [
            'post_type' => 'cart_frse',
        ];
        $post_id = wp_insert_post( $arr );
        if( !$post_id ){
            echo wp_send_json_error([ 'errorMessage' => 'Wystąpił błąd techniczny. podczas dodawania produktu do koszyka. Zgłoś problem administratorowi.' ]);
            exit();
        }

        $category = frse_product_category($_POST['product_id']);
        wp_update_post([
            'ID'           => $post_id,
            'post_title'   => $product->post_title,
        ]);
        wp_publish_post( $post_id );
        wp_set_object_terms( $post_id, array( frse_product_category_name_by_product_id($_POST['product_id']) ), 'frse_category' );
        wp_set_object_terms( $post_id, array( 'start' ), 'status_order_item' );
        update_field('total_price', $totalPrice, $post_id);
        update_field('product_id', $_POST['product_id'], $post_id);
        update_field('count', $_POST['order_count'], $post_id);
        update_field('color', $_POST['order_color'], $post_id);

        if( $_FILES ){

        }
        foreach( $_FILES as $file ){
            frse_upload_project_to_item( $post_id, $file );
        }


        $update = frse_user_budget_remove( $category->term_id, $totalPrice );

        //Dodawanie projektu
        /*$posted_data =  isset( $_POST ) ? $_POST : array();
        $file_data = isset( $_FILES ) ? $_FILES : array();
        $data = array_merge( $posted_data, $file_data );
        $response = array();
        //$uploaded_file = wp_handle_upload( $data['ibenic_file_upload'], array( 'test_form' => false ) );
        print_log( $_FILES );
        echo '---';
        /*
        if( $uploaded_file && ! isset( $uploaded_file['error'] ) ) {
            $response['response'] = "SUCCESS";
            $response['filename'] = basename( $uploaded_file['url'] );
            $response['url'] = $uploaded_file['url'];
            $response['type'] = $uploaded_file['type'];
        } else {
            $response['response'] = "ERROR";
            $response['error'] = $uploaded_file['error'];
        }
        echo json_encode( $response );
        die();
        */

        echo wp_send_json([
            'success' => true,
            'totalPrice' => $totalPrice,
            'order_item_id' => $post_id
        ]);
        exit();
    }


    function frse_user_budget_remove( $cat_id, $price ){
        $current_budget = frse_get_user_budget_by_category_id( $cat_id );
        if( have_rows('categories', 'user_'.get_current_user_id()) ):
            $row_index = 0;
            while ( have_rows('categories', 'user_'.get_current_user_id()) ) : the_row();
                $row_index++;
                if( get_sub_field('category') == (int)$cat_id ){
                    $curr_bud = get_sub_field('budget');
                    $new_budget = (int)$curr_bud - (int)$price;
                    return update_sub_field( array("categories", $row_index, "budget"), $new_budget, 'user_'.get_current_user_id());
                }
            endwhile;
        endif;
    }
