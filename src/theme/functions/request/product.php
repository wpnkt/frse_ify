<?php

    add_action('wp_ajax_product', 'frse_request_product');
    function frse_request_product() {
        $product = frse_get_normalize_product_by_id( $_POST['product_id'] );
        echo wp_send_json($product);
        exit();
    }

