<?php

    function frse_get_normalize_order_item_for_order_list_by_id( $id ){
        $post = get_post( $id );
        $fields = get_fields( $id );
        $statuses = get_the_terms( $id, 'status_order_item' );
        $items = [];
        $author = get_userdata($post->post_author);
        $category = frse_get_category_by_post_id( $id );
        $product = get_post( $fields['product_id'] );
        $ret = [
            'id' => $post->ID,
            'name' => $product->post_title,
            'count' => $fields['count'],
            'total_price' => $fields['total_price'],
            'link' => get_permalink( $post->ID ),
            'status' => [
                'name' => $statuses[0]->name
            ],
            'category' => [
                'name' => $category->name
            ],
        ];

        return $ret;
    }

    function frse_get_normalize_order_list_items_by_order_list_id( $order_id ){
        $items = [];
        $params = [
            'numberposts' => -1,
            'post_type' => 'order_item',
            'meta_key'		=> 'order_id',
	        'meta_value'	=> $order_id
        ];
        $posts = get_posts( $params );
        foreach( $posts as $post ){
            $items[] = frse_get_normalize_order_item_for_order_list_by_id( $post->ID );
        }
        return $items;
    }

    function frse_get_normalize_order_list_by_id( $id ){
        $post = get_post( $id );
        $fields = get_fields( $id );
        $statuses = get_the_terms( $id, 'status_order_list' );
        $items = [];
        $author = get_userdata($post->post_author);
        $category = frse_get_category_by_post_id( $id );
        $ret = [
            'id' => $post->ID,
            'title' => $post->post_title,
            'date' => $post->post_date,
            'author_name' => $author->data->user_nicename,
            'author_mail' => $author->data->user_email,
            //'prefer_delivery_date' => $fields['prefer_delivery_date'],
            'status' => [
            //    'name' => $statuses[0]->name
            ],
            'category' => [
                'name' => $category->name
            ],
            'items' => frse_get_normalize_order_list_items_by_order_list_id( $id ),
        ];
        return $ret;
    }

    function frse_order_lists_normalize( $lists ){
        $normalize_posts = [];
        foreach( $lists as $list ){
            $normalize_posts[] = frse_get_normalize_order_list_by_id( $list->ID );
        }
        return $normalize_posts;
    }

    function frse_get_normalize_order_lists(){
        $params = [
            'numberposts' => -1,
            'post_type' => 'order_list'
        ];
        $role = frse_user_role();
        switch( $role ){
            case "administrator": //pobiera wszystko
                break;
            case "frse_admin":  //brak dostepu
                $params['numberposts'] = 0;
                break;
            case "frse_supervisor": //pobiera wszystko
                break;
            case "frse_customer_in": //pobiera swojego autorstwa
                //$params['author'] = get_current_user_id();
                break;
            case "frse_producer": //pobiera z kategori
                $params['tax_query'] = [
                    [
                        'taxonomy' => 'frse_category',
                        'field' => 'slug',
                        'terms' => frse_current_user_categories_slugs(),
                    ]
                ];
                break;
            default:
                break;
        }
        $posts = get_posts( $params );
        $lists = frse_order_lists_normalize( $posts );
        return $lists;
    }


   // frse_get_normalize_order_lists
   /* function test(){
        $list = frse_get_normalize_order_lists( );
        //print_log( $list);
    }
    add_action('init', 'test');*/

