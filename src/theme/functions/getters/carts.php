<?php

    function get_normalize_cart_item_by_id( $item_id ){
        $post = get_post( $item_id );
        $fields = get_fields( $item_id );
        $product = get_post($fields['product_id']);
        $product_fields = get_fields($product->ID);
        return [
            'id' => $item_id,
            'title' => $post->post_title,
            'count' => $fields['count'],
            'total_price' => $fields['total_price'],
            'date' => $post->post_date,
            'projects_count' => count($fields['projects']),
            'link' => get_permalink($item_id),
            'price_for' => $product_fields['price_for']
        ];
    }

    function frse_get_user_carts_group_by_category(){
        $carts = [];
        $cat_slugs = frse_get_user_categories_slugs();
        foreach( $cat_slugs as $cat_slug ){
            $param = [
                'numberposts' => -1,
                'post_type' => 'cart_frse',
                'author' =>  get_current_user_id(),
                'fields' => 'ids',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'frse_category',
                        'field'    => 'slug',
                        'terms'    => $cat_slug
                    )
                ),
            ];
            $items = get_posts($param);
            $items_normalize = [];
            foreach( $items as $item ){
                $items_normalize[] = get_normalize_cart_item_by_id($item);
            }
            $cat =  get_term_by( 'slug', $cat_slug, 'frse_category' );
            $carts[] = [
                'category' => [
                    'name' => $cat->name,
                    'id' => $cat->term_id
                ],
                'items' => $items_normalize
            ];
        }
        return $carts;
    }

    function frse_get_normalize_user_carts(){
        $carts = frse_get_user_carts_group_by_category();
        return $carts;
    }

    //////
   /* function test(){
        $arr = frse_get_normalize_user_carts(  );
        print_log( $arr, "PRodukt" );
    }

    add_action('init', 'test');*/
