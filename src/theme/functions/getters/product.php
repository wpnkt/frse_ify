<?php

    function frse_get_normalize_product_by_id( $prod_id ){

        $prod = get_post( $prod_id );
        if( $prod->post_type != 'product' && $prod->post_type != 'cart_frse' ){ return false; }

        $category = frse_get_category_by_post_id( $prod_id );
        $fields = get_fields( $prod_id );
        $author = get_userdata($prod->post_author);

        $product_colors = [];
        $filter_colors = [];
        foreach( $fields['colors'] as $color ){
            $product_colors[] = $color['color'];
            $filter_colors[] = $color['filter_color'];
        }

        $gallery = [];
        foreach( $fields['gallery'] as $image ){
            $gallery[] = $image['url'];
        }

        $ret = [
            'id' => $prod->ID,
            'title' => $prod->post_title,
            'link' => get_permalink($prod->ID),
            'author_name' => $author->data->user_nicename,
            'author_email' => $author->data->user_email,
            'colors' => $fields['colors'],
            'product_colors' => $product_colors,
            'filter_colors' => $filter_colors,
            'label' => $fields['label'],
            'prices' => $fields['prices'],
            'default_price' => $fields['prices'][0]['price'],
            'description' => $prod->post_content,
            'price_for' => $fields['price_for'],
            'thumbnail_url' => get_the_post_thumbnail_url($prod->ID),
            'gallery' => $gallery,
            'category'=> $category,
        ];

        return $ret;
    }

    /*
    function test(){
        $prod = frse_get_normalize_product_by_id( 32 );
        print_log( $prod, "PRodukt" );
    }

    add_action('init', 'test');*/
