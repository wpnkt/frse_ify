<?php

    function frse_get_category_by_post_id( $post_id ){
        $categories = get_the_terms( $post_id, 'frse_category' );
        return $categories[0];
    }

    function frse_category_in_categories( $category_id, $categories ){
        foreach( $categories as $category ){
            if( $category_id == $category['category'] ){
                return $category;
            }
        }
        return false;
    }

    function frse_get_products_by_category_id( $category_id ){
        $products = get_posts( array(
            'posts_per_page' => -1,
            'post_type' => 'product',
            'tax_query' => array(
                array(
                    'taxonomy' => 'frse_category',
                    'field' => 'term_id',
                    'terms' => $category_id,
                )
            )
        ) );
        return $products;
    }

    function frse_get_products_count_by_category_id( $category_id ){
        $products = frse_get_products_by_category_id( $category_id );
        return count($products);
    }

    function frse_get_user_budget_by_category_id( $category_id ){
        $curr_user_id = get_current_user_id();
        $categories = get_field('categories', 'user_'.$curr_user_id );
        foreach( $categories as $cat ){
            if( $category_id == $cat['category'] ){
                return $cat['budget'];
            }
        }
        return 0;
    }

    function frse_single_category( $id ){
        $category = [
            'category_id' => $id,
            'id' => $id,
            'term' => get_term( $id, 'frse_category', $output = OBJECT ),
            'fields' => get_fields( 'frse_category_' . $id ),
            'budget' => frse_get_user_budget_by_category_id($id),
            'products_count' => frse_get_products_count_by_category_id($id),
            'link' => get_term_link( $id, 'frse_category' )
        ];
        return $category;
    }

    function frse_current_user_categories(){
        $curr_user_id = get_current_user_id();
        $categories = get_field('categories', 'user_'.$curr_user_id );
        $array = [];
        foreach( $categories as $category ){
            $array[] = frse_single_category( $category['category'] );
        }
        return $array;
    }

    function frse_current_user_category(){
        $curr_user_id = get_current_user_id();
        $category_id = get_field('current_category', 'user_'.$curr_user_id );
        $categories = get_field('categories', 'user_'.$curr_user_id );
        if( $category = frse_category_in_categories( $category_id, $categories ) ){
            return [
                'term' => get_term( $category_id, 'frse_category', $output = OBJECT ),
                'category_id' => $category_id,
                'budget' => $category['budget'],
            ];
        }else{
            return NULL;
        }
    }

    function frse_current_user_categories_slugs(){
        $categories = frse_current_user_categories();
        $slugs = [];
        foreach( $categories as $category ){
            $slugs[] = $category['term']->slug;
        }
        return $slugs;
    }

    function frse_get_product_each_price( $product_id, $count = 1 ){
        $prices = get_field( 'prices', $product_id );
        $price_each = 0;
        foreach( $prices as $price ){
            if( $count > (int)$price['from'] ){
                $price_each = (int)$price['price'];
            }
        }
        return $price_each;
    }

    function frse_get_product_total_price( $product_id, $count = 1 ){
        $each_price = frse_get_product_each_price( $product_id, $count );
        return (int)$each_price * (int)$count;
    }

    function frse_product_category( $prod_id ){
        $categories = get_the_terms( $prod_id, 'frse_category' );
        return $categories[0];
    }

    function frse_product_category_name_by_product_id( $prod_id ){
        $categories = get_the_terms( $prod_id, 'frse_category' );
        return $categories[0]->name;
    }

    function frse_get_user_categories_ids(){
        $curr_user_id = get_current_user_id();
        $categories_fields = get_field( 'categories', 'user_'.$curr_user_id );
        $ids = [];
        foreach( $categories_fields as $field ){
            $ids[] = $field['category'];
        }
        return $ids;
    }
    function frse_get_user_categories_slugs(){
        $ids = frse_get_user_categories_ids();
        $slugs = [];
        foreach( $ids as $id ){
            $term = get_term( $id );
            $slugs[] = $term->slug;
        }
        return $slugs;
    }

    function frse_get_cart_products_by_category_id( $cat_id ){
        $arr = [
            'post_type' => 'cart_frse',
            'numberposts' => -1,
            'author' => get_current_user_id(),
            'tax_query' => array(
                'taxonomy' => 'frse_category',
                'field' => 'id',
                'terms' => $cat_id,
            )
        ];
        $posts = get_posts( $arr );
        return $posts;
    }
