<?php 

    function frse_get_order_item_normalize_for_receipt( $item_id ){
        $post = get_post( $item_id );       
        $fields = get_fields($item_id); 
        $status = get_the_terms( $post->ID, 'status_order_item' );
        $arr = [
            'order_item_id' => $post->ID,
            'title' => $post->post_title,
            'count' => $fields['count'],
            'total_price' => $fields['total_price'],
            'author_email' => get_the_author_meta( 'user_email', $post->post_author ),
            'order_item_link' => get_permalink($post->ID),
            'status_name' => $status[0]->name,
        ];
        return $arr;
    }

    /**
     * Return posts order_items by receipt ID
     */
    function frse_get_receipt_order_items( $receipt_id ){
        $arr = [
            'numberposts' => -1,
            'post_type' => 'order_item',
            'meta_key'		=> 'receipt_id',
	        'meta_value'	=> $receipt_id
        ];
        $posts = get_posts($arr);
        return $posts;
    }

    function frse_normalize_order_items_for_receipt( $items ){
        $normalize_posts = [];
        foreach( $items as $item ){
            $normalize_posts[] = frse_get_order_item_normalize_for_receipt( $item->ID );
        }
        return $normalize_posts;
    }

    //JEDNA PRZSYLKA = zestaw zamowionych produktów
    function frse_get_receipt_normalize_by_id( $receipt_id ){
        $post_receipt = get_post( $receipt_id );
        $fields_receipt = get_fields( $receipt_id );
        //$status_receipt = get_the_terms( $receipt_id, 'status_receipt' );
        $items = frse_get_receipt_order_items( $receipt_id );
        $products = frse_normalize_order_items_for_receipt( $items );
        $datetime = get_post_time( 'Y-m-d H:i:s', false, $receipt_id );
        $arr = [
            'id' => $post_receipt->ID,
            'title' => $post_receipt->post_title,
            'datetime_add' => $datetime,
            'delivery_estimate' => $fields_receipt['delivery_estimate'],
            'delivery_number' => $fields_receipt['delivery_number'],
            //'status' => $status_receipt[0],
            'items' => $products
        ];
        return $arr;
    }

    function frse_receipts_normalize( $receipts ){
        $normalize_posts = [];
        foreach( $receipts as $receipt ){
            $normalize_posts[] = frse_get_receipt_normalize_by_id( $receipt->ID );
        }
        return $normalize_posts;
    }

    function frse_get_receipts(){
        $arr = [
            'numberposts' => -1,
            'post_type' => 'receipt'
        ];
        $role = frse_user_role();
        switch( $role ){
            case "administrator": //pobiera wszystko
                break;
            case "frse_admin":  //brak dostepu
                $arr['numberposts'] = 0;
                break;
            case "frse_supervisor": //pobiera wszystko
                break;
            case "frse_customer_in": //pobiera swojego autorstwa
                $arr['author'] = get_current_user_id();
                break;
            case "frse_producer": //pobiera z kategori
                $arr['tax_query'] = [
                    [
                        'taxonomy' => 'frse_category',
                        'field' => 'slug',
                        'terms' => frse_current_user_categories_slugs(),
                    ]
                ];
                break;
            default:
                break;
        }
        $posts = get_posts( $arr );
        $receipts = frse_receipts_normalize( $posts );
        return $receipts;
    }

    function frse_receipts_remove(){
    //usuniecie calej przesylki - tylko wykonawca 
    }

    function frse_receipts_remove_item(){
    //usuniecie jednego przedmiotu z przesylki - tylko wykonawca 
    }

    function frse_receipts_add_item(){
    //dodanie jednego przedmiotu do przesylki - tylko wykonawca 
    }
