<?php 

    add_role(
        'frse_supervisor',
        __( 'Supervisor FRSE' ),
        array(
            'read'         => true,
            'edit_posts'   => true,
        )
    );
