<?php 

    add_role(
        'frse_customer_in',
        __( 'Klient wewnętrzny FRSE' ),
        array(
            'read'         => true,
            'edit_posts'   => true,
        )
    );
