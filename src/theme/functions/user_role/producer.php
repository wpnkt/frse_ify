<?php 

    add_role(
        'frse_producer',
        __( 'Wykonawca / Producent FRSE' ),
        array(
            'read'         => true,
            'edit_posts'   => true,
        )
    );
