<?php 

    add_role(
        'frse_admin',
        __( 'Administrator WP' ),
        array(
            'read'         => true,
            'edit_posts'   => true,
        )
    );
