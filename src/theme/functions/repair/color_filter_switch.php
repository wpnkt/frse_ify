<?php 

add_action( 'acf/input/admin_enqueue_scripts', function() {
  wp_enqueue_script( 'acf-custom-colors', get_template_directory_uri() . '/functions/repair/color_filter_switch.js', 'acf-input', '1.0', true );
});
