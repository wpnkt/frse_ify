jQuery(document).ready(function($) {
    if( acf.fields.color_picker ) {
        // custom colors
        var palette = ['#111111', '#333333', '#555555', '#777777', '#999999', '#cccccc'];

        // when initially loaded find existing colorpickers and set the palette
        acf.add_action('load', function() {
        $('input.wp-color-picker').each(function() {
            $(this).iris('option', 'palettes', palette);
        });
        });

        // if appended element only modify the new element's palette
        acf.add_action('append', function(el) {
            $(el).find('input.wp-color-picker').iris('option', 'palettes', palette);
            var x = $(el).find('input.wp-color-picker');
            console.log( x );
            console.log("SWITCH2222", x );
        });
    }

    

});


console.log("SWITCH");