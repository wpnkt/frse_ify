<?php 

    //front end formular - wrong password
    add_action( 'wp_login_failed', 'custom_login_failed' );
    function custom_login_failed( $username )
    {
        $referrer = wp_get_referer();

        if ( $referrer && ! strstr($referrer, 'wp-login') && ! strstr($referrer,'wp-admin') )
        {
            wp_redirect( add_query_arg('login', 'failed', $referrer) );
            echo "DUP{A";
            exit;
        }
    }

    //page with redirect to default url
    function dashboard_url(){
        return home_url();
    }

    //get redirect url for logged user
    function redirect_url_for_users(){
        $user = frse_user_role();
        $url = '';
        switch ($user) {
            case 'frse_admin':
                return 'frse_admin';
                break;
            case 'frse_customer_in':
                return 'frse_admin';
                break;
            case 'frse_producer':
                return 'frse_admin';
                break;
            case 'frse_producer':
                return 'frse_admin';
                break;
            default:
                return home_url();
        }
    }
