<?php 

    function remove_menus() {
        if( !current_user_can('administrator') ){
            remove_menu_page( 'index.php' );                  //Dashboard
            remove_menu_page( 'jetpack' );                    //Jetpack* 
            remove_menu_page( 'edit.php' );                   //Posts
            remove_menu_page( 'upload.php' );                 //Media
            remove_menu_page( 'edit.php?post_type=page' );    //Pages
            remove_menu_page( 'edit-comments.php' );          //Comments
            remove_menu_page( 'themes.php' );                 //Appearance
            remove_menu_page( 'plugins.php' );                //Plugins
            remove_menu_page( 'users.php' );                  //Users
            remove_menu_page( 'tools.php' );                  //Tools
            remove_menu_page( 'options-general.php' );        //Settings
            remove_menu_page( 'edit.php?post_type=acf-field-group' );
            remove_menu_page( 'edit.php?post_type=complaint' );
            remove_menu_page( 'edit.php?post_type=invoice' );
            remove_menu_page( 'edit.php?post_type=order_item' );
            remove_menu_page( 'edit.php?post_type=order_list' );
            remove_menu_page( 'edit.php?post_type=project' );
            remove_menu_page( 'edit.php?post_type=receipt' );
            remove_menu_page( 'edit.php?post_type=shipping' );
            remove_menu_page( 'edit.php?post_type=user_profile' );
        }
    }
    add_action( 'admin_menu', 'remove_menus' );
