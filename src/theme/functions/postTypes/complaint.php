<?php 

add_action('init', 'create_custom_post_type_complaint');
    function create_custom_post_type_complaint(){

        $labels = array(
            'name' => _x('Reklamacje', 'post type general name'),
            'singular_name' => _x('Reklamacje', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Reklamacje'),
            'add_new_item' => __('Dodaj nowy Reklamacje'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych Reklamacje'),
            'not_found_in_trash' => __('Nie znaleziono żadnych Reklamacje w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'complaint'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'complaint',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category', 'status_complaint'
            ),
            'supports' => array('title','editor'),
            'has_archive' => true,
        );
        register_post_type('complaint',$args);
    }
