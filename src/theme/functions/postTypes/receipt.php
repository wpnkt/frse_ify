<?php 

//Przesylka - zawiera liste zamowionych przedmiotow - przedmiot->field->receipt_id

add_action('init', 'create_custom_post_type_receipt');
    function create_custom_post_type_receipt(){

        $labels = array(
            'name' => _x('Protokoły odbioru', 'post type general name'),
            'singular_name' => _x('Protokoły odbioru', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Protokoły odbioru'),
            'add_new_item' => __('Dodaj nowy Protokoły odbioru'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych Protokoły odbioru'),
            'not_found_in_trash' => __('Nie znaleziono żadnych Protokoły odbioru w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'receipt'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'receipt',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category'
            ),
            'supports' => array('title'),
            'has_archive' => true,
        );
        register_post_type('receipt',$args);
    }
