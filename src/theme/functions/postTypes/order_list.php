<?php 

add_action('init', 'create_custom_post_type_order_list');
    function create_custom_post_type_order_list(){

        $labels = array(
            'name' => _x('Zamówienia - listy', 'post type general name'),
            'singular_name' => _x('Zamówienia - listy', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Zamówienia - listy'),
            'add_new_item' => __('Dodaj nowy Zamówienia - listy'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych Zamówienia - listy'),
            'not_found_in_trash' => __('Nie znaleziono żadnych Zamówienia - listy w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'order_list'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'order_list',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category', 'status_order_list'
            ),
            'supports' => array('title','editor'),
            'has_archive' => true,
        );
        register_post_type('order_list',$args);
    }
