<?php 

add_action('init', 'create_custom_post_type_cart');
    function create_custom_post_type_cart(){

        $labels = array(
            'name' => _x('Koszyki', 'post type general name'),
            'singular_name' => _x('Koszyki', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Koszyki'),
            'add_new_item' => __('Dodaj nowy Koszyki'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych Koszyki'),
            'not_found_in_trash' => __('Nie znaleziono żadnych Koszyki w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'cart_frse'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'cart_frse',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category'
            ),
            'supports' => array('title'),
            'has_archive' => true,
        );
        register_post_type('cart_frse',$args);
    }
