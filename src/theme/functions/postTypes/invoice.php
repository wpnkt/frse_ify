<?php 

add_action('init', 'create_custom_post_type_invoice');
    function create_custom_post_type_invoice(){

        $labels = array(
            'name' => _x('Faktury', 'post type general name'),
            'singular_name' => _x('Faktury', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Faktury'),
            'add_new_item' => __('Dodaj nowy Faktury'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych Faktury'),
            'not_found_in_trash' => __('Nie znaleziono żadnych Faktury w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'invoice'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'invoice',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category', 'status_invoice'
            ),
            'supports' => array('title','editor'),
            'has_archive' => true,
        );
        register_post_type('invoice',$args);
    }
