<?php 

add_action('init', 'create_custom_post_type_order_item');
    function create_custom_post_type_order_item(){

        $labels = array(
            'name' => _x('Zamówione produkty', 'post type general name'),
            'singular_name' => _x('Zamówione produkty', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Zamówione produkty'),
            'add_new_item' => __('Dodaj nowy Zamówione produkty'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych Zamówione produkty'),
            'not_found_in_trash' => __('Nie znaleziono żadnych Zamówione produkty w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'order_item'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'order_item',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category', 'status_order_item'
            ),
            'supports' => array('title','editor'),
            'has_archive' => true,
        );
        register_post_type('order_item',$args);
    }
