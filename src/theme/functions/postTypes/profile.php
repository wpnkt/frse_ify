<?php 

add_action('init', 'create_custom_post_type_user_profile');
    function create_custom_post_type_user_profile(){

        $labels = array(
            'name' => _x('Profile i Budżety', 'post type general name'),
            'singular_name' => _x('Profile i Budżety', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Profile i Budżety'),
            'add_new_item' => __('Dodaj nowy Profile i Budżety'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych Profile i Budżety'),
            'not_found_in_trash' => __('Nie znaleziono żadnych Profile i Budżety w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'user_profile'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'user_profile',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category'
            ),
            'supports' => array('title'),
            'has_archive' => true,
        );
        register_post_type('user_profile',$args);
    }
