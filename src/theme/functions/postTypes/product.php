<?php 

    add_action('init', 'create_custom_post_type_product');
    function create_custom_post_type_product(){

        $labels = array(
            'name' => _x('Produkty', 'post type general name'),
            'singular_name' => _x('Produkt', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Produkt'),
            'add_new_item' => __('Dodaj nowy produkt'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych produktów'),
            'not_found_in_trash' => __('Nie znaleziono żadnych produktów w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'product'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'product',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category'
            ),
            'supports' => array('title','editor','thumbnail'),
            'has_archive' => true,
        );
        register_post_type('product',$args);
    }
