<?php 

    //add_action('init', 'create_custom_post_type_project');
    function create_custom_post_type_project(){

        $labels = array(
            'name' => _x('Projekt', 'post type general name'),
            'singular_name' => _x('Projekt', 'post type singular name'),
            'add_new' => _x('Dodaj nowy', 'Projekt'),
            'add_new_item' => __('Dodaj nowy Projekt'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych Projekt'),
            'not_found_in_trash' => __('Nie znaleziono żadnych Projekt w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'project'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'project',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(
                'frse_category'
            ),
            'supports' => array('title','editor'),
            'has_archive' => true,
        );
        register_post_type('project',$args);
    }
