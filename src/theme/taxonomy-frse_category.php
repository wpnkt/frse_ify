<?php get_header(); ?>

<?php 

if( in_array( frse_user_role(), [ 'frse_customer_in'] ) ){
    frse_set_current_category( get_queried_object()->term_id );
}

if( in_array( frse_user_role(), ['administrator', 'frse_supervisor', 'frse_customer_in'] ) ){
    view( 'category_products' );
}else{
    //wp_redirect( get_home_ur( '?page=10' ) );
    //exit();
}
?>

<?php get_footer(); ?>