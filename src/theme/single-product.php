<?php get_header(); ?>

<?php 
    $role = frse_user_role();
    switch( $role ){
        case "administrator":
            view('product');
            break;
        case "frse_admin":
            wp_redirect( home_url( '/wp-admin/' ) );
            exit();
            break;
        case "frse_supervisor":
            view('product');
            break;
        case "frse_customer_in":
            view('product');
            break;
        case "frse_producer":
            wp_redirect( home_url() );
            exit();
            break;
        default:
			wp_redirect( home_url() );
            exit();
            break;
    } 
?>

<?php get_footer(); ?>
