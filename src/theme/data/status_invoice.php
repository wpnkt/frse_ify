<?php 

function data_register_status_invoice(){
    $terms = [
        [
            'name' => 'Oczekuje',
            'slug' => 'wait',
        ],
        [
            'name' => 'Odrzucona',
            'slug' => 'rejected',
        ],
        [
            'name' => 'Zaakceptowana',
            'slug' => 'accepted',
        ],
    ];
    register_taxonomies_by_array( $terms, 'status_invoice' );
}
