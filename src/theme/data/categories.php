<?php 

function data_register_categories(){
    $status_invoice_terms = [
        'frse_cat_gadgets' => 'Gadżety',
        'frse_cat_others' => 'Inne'
    ];
    foreach( $status_invoice_terms as $term_slug => $term_name ){
        if(!term_exists($term_name, 'frse_category')){
            wp_insert_term($term_name, 'frse_category', ['slug' => $term_slug]);
        }
    }
}
