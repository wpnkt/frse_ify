<?php 

function data_register_status_shipping(){
    $terms = [
        [
            'name' => 'Wysłana',
            'slug' => 'start',
        ],
        [
            'name' => 'Dostarczona',
            'slug' => 'final',
        ]
    ];
    register_taxonomies_by_array( $terms, 'status_shipping' );
}
