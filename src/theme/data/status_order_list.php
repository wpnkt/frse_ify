<?php 

function data_register_status_order_list(){
    $terms = [
        [
            'name' => 'Koszyk',
            'slug' => 'in_cart',
        ],
        [
            'name' => 'W trakcie realizcji',
            'slug' => 'in_progress',
        ],
        [
            'name' => 'Zrealizowane',
            'slug' => 'final',
        ]

    ];
    register_taxonomies_by_array( $terms, 'status_order_list' );
}
