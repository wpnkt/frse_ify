<?php 

function register_taxonomies_by_array( $terms, $taxonomy ){
    foreach( $terms as $term ){
        if ( ! term_exists( $term['slug'], $taxonomy ) ) {
            wp_insert_term( $term['name'], $taxonomy, array( 'slug' => $term['slug'] ) );
        }  
    }
}

function register_users_by_array( $users ){
    foreach( $users as $u ){
        if ( ! username_exists( $u['name'] ) ) {
            $user_id = wp_create_user( $u['name'], $u['password'], $u['email'] );
            $user = new WP_User( $user_id );
            $user->set_role( $u['role'] );
        }  
    }
}

function register_default_data(){
    data_register_users();
    data_register_status_invoice();
    data_register_status_order_list();
    data_register_status_order_item();
    data_register_status_project();
    data_register_status_shipping();
    data_register_status_complaint();
    data_register_categories();
}

add_action( 'init', 'register_default_data', 99 );
