<?php 

function data_register_status_order_item(){
    //PRE: oczekuje na wizualizacje / koszyk
    //oczekuje na projekt od wykonawcy
    //opiniowanie przez klienta
    //klient dodal korekty - oczekiwanie na poprawki klient 
    //klient zaakceptowal projekt - opiniowanie przez admina
    //admin dodal korekty - oczekiwanie na poprawki admina  
    //admin zaakceptowal projekt - w trakcie produkcji
    //wykonawca dodal presylke - w trakcie przesylki
    //klient/admin dodal protokul odbioru - zaakceptowane

    //klient dodal reklamacje - tworzony jest dodatkowy rekord reklamacji - zamowienie przechodzi do etapu w trakcie produkcji


    $terms = [
        [//PRE: oczekuje na wizualizacje / koszyk
            'name' => 'Oczekuje na wizualizacje',
            'slug' => 'start',
        ],
        [//oczekuje na projekt od wykonawcy
            'name' => 'Oczekuje na dodanie projektu przez wykonawce',
            'slug' => 'wait_for_project',
        ],
        [////opiniowanie przez klienta
            'name' => 'Oczekuje na opinię klienta',
            'slug' => 'wait_for_customer_opinion',
        ],
        [////klient dodal korekty - oczekiwanie na poprawki klient 
            'name' => 'Oczekuje na wprowadzenie poprawek klienta',
            'slug' => 'wait_for_correction_from_customer',
        ],
        [////klient zaakceptowal projekt - opiniowanie przez admina
            'name' => 'Oczekuje na opinię administratora',
            'slug' => 'wait_for_admin_opinion',
        ],
        [////admin dodal korekty - oczekiwanie na poprawki admina  
            'name' => 'Oczekuje na wprowadzenie poprawek administratora',
            'slug' => 'wait_for_correction_from_admin',
        ],
        [////admin zaakceptowal projekt - w trakcie produkcji
            'name' => 'W trakcie produkcji',
            'slug' => 'in_production',
        ],
        [////wykonawca dodal presylke - w trakcie przesylki
            'name' => 'W trakcie przesyłki',
            'slug' => 'shipping',
        ],
        [////klient/admin dodal protokul odbioru - zaakceptowane
            'name' => 'Zaakceptowane',
            'slug' => 'final',
        ]
    ];
    register_taxonomies_by_array( $terms, 'status_order_item' );
}
