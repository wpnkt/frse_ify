<?php 

function data_register_status_complaint(){
    $terms = [
        [
            'name' => 'W trakcie realizacji',
            'slug' => 'in_progress',
        ],
        [
            'name' => 'Zrealizowana',
            'slug' => 'final',
        ]
    ];
    register_taxonomies_by_array( $terms, 'status_complaint' );
}
