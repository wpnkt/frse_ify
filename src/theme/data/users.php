<?php 

function data_register_users(){
    $users = [
        [
            'name' => 'frse_admin',
            'password' => 'frse',
            'email' => 'frse_admin@frse.pl',
            'role' => 'frse_admin' 
        ],
        [ 
            'name' => 'frse_supervisor',
            'password' => 'frse',
            'email' => 'frse_supervisor@frse.pl',
            'role' => 'frse_supervisor'
        ],
        [
            'name' => 'frse_customer',
            'password' => 'frse',
            'email' => 'frse_customer@frse.pl',
            'role' => 'frse_customer_in'
        ],
        [
            'name' => 'frse_producer',
            'password' => 'frse',
            'email' => 'frse_producer@frse.pl',
            'role' => 'frse_producer'
        ]
    ];
    register_users_by_array( $users );
}
