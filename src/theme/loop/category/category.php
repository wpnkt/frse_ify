<?php 
global $loop_category;
$category = $loop_category;
?>

<a class="grid__item category" href="<?php echo $category["link"] ?>"> 
    <div class="category__title">
        <h1 class="title--icon">
            <span class="title__icon icon icon--list"></span>
            <span class="title__text"><?php echo $category["term"]->name ?></span>
        </h1>
    </div>
    <div class="category__data">
        <p class="category__count">
            <span class="category__txt"><?php echo $category['products_count']; ?></span>
            <small class="category__info">produktów</small>
        </p>
    </div>
    <div class="category__actions">
        <button class="btn--secondary btn--cursor" >
            <span class="title__text">Zobacz</span>
            <span class="title__icon icon icon--arrow-right"> > </span>
        </button>
    </div>
</a>