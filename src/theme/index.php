<?php get_header(); ?>

<h1>INDEX.php</h1>

<!-- container -->
<div class="container">	
	<!-- site-content -->
	<div class="site-content">

		<!-- main-column -->
		<div class="main-column grid">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					get_template_part( 'content', get_post_format() );
				endwhile;
				?>
			<?php
			else :
				get_template_part( 'content', 'none' );
			endif;
			?>
		</div>
		<!-- /main-column -->

	</div>
	<!-- /site-content -->

</div>
<!-- /container -->

<?php get_footer(); ?>
